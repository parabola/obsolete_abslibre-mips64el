# $Id: PKGBUILD 204259 2014-01-15 20:56:02Z bpiotrowski $
# Maintainer:  Ionut Biru <ibiru@archlinux.org>
# Maintainer:  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Tom Newsom <Jeepster@gmx.co.uk>
# Contributor: Paul Mattal <paul@archlinux.org>

pkgname=ffmpeg
pkgver=2.1.3
pkgrel=1
epoch=1
pkgdesc='Complete and free Internet live audio and video broadcasting solution'
arch=('i686' 'x86_64' 'mips64el')
url='http://ffmpeg.org/'
license=('GPL')
depends=(
      'alsa-lib' 'bzip2' 'fontconfig' 'gnutls' 'gsm' 'lame' 'libass'
      'libbluray' 'libmodplug' 'libpulse' 'libtheora' 'libvorbis' 'libvpx'
      'opencore-amr' 'openjpeg' 'opus' 'rtmpdump' 'schroedinger' 'sdl' 'speex'
      'v4l-utils' 'x264' 'xvidcore' 'zlib' 'libxfixes'
      )
if [ "${CARCH}" != "mips64el" ]; then
    depends+=('libva')
    makedepends=('yasm' 'libvdpau')
fi
source=(http://ffmpeg.org/releases/$pkgname-$pkgver.tar.bz2
        ffmpeg-2.1.1-freetype2.patch)
md5sums=('711b795bbc7b527c0f4a1828f324fd5a'
         '4b5dd079a40f44f4e0d00cdbc9d52ec3')

prepare() {
  cd $pkgname-$pkgver
  patch -p1 -i ../ffmpeg-2.1.1-freetype2.patch
}

build() {
  cd $pkgname-$pkgver

  if [ "$CARCH" = "mips64el" ] ; then
      # Disable runtime CPU detection, video acceleration APIs our hardware doesn't support and ISAs our CPUs don't support.
      extraconf="--disable-runtime-cpudetect --disable-vdpau --disable-vaapi --disable-mips32r2 --disable-mipsdspr1 --disable-mipsdspr2"
  else
      extraconf=""
  fi

  ./configure \
    --prefix=/usr \
    --disable-debug \
    --disable-static \
    --enable-avresample \
    --enable-dxva2 \
    --enable-fontconfig \
    --enable-gnutls \
    --enable-gpl \
    --enable-libass \
    --enable-libbluray \
    --enable-libfreetype \
    --enable-libgsm \
    --enable-libmodplug \
    --enable-libmp3lame \
    --enable-libopencore_amrnb \
    --enable-libopencore_amrwb \
    --enable-libopenjpeg \
    --enable-libopus \
    --enable-libpulse \
    --enable-librtmp \
    --enable-libschroedinger \
    --enable-libspeex \
    --enable-libtheora \
    --enable-libv4l2 \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libx264 \
    --enable-libxvid \
    --enable-pic \
    --enable-postproc \
    --enable-runtime-cpudetect \
    --enable-shared \
    --enable-swresample \
    --enable-vdpau \
    --enable-version3 \
    --enable-x11grab \
    $extraconf

  make
  make tools/qt-faststart
  make doc/ff{mpeg,play,server}.1
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install install-man
  install -Dm755 tools/qt-faststart "$pkgdir"/usr/bin/qt-faststart
}

# vim:set ts=2 sw=2 et:
