# $Id: PKGBUILD 199350 2013-11-11 16:08:18Z heftig $
# Maintainer: Ionut Biru <ibiru@archlinux.org>

pkgbase=pygobject
pkgname=(python-gobject python2-gobject pygobject-devel)
pkgver=3.10.2
pkgrel=1
arch=(i686 x86_64 mips64el)
url="https://live.gnome.org/PyGObject"
license=(LGPL)
makedepends=(python python2 python-cairo python2-cairo gobject-introspection)
source=(http://ftp.gnome.org/pub/gnome/sources/$pkgbase/${pkgver:0:4}/$pkgbase-$pkgver.tar.xz)
sha256sums=('75608f2c4052f0277508fc79debef026d9e84cb9261de2b922387c093d32c326')

prepare() {
  mkdir build-py2 build-py3 devel
  cd ${pkgbase}-${pkgver}
  sed -i '/Werror=format/d' configure # gcc 4.8
}

build() {
  cd build-py2
  ../${pkgbase}-${pkgver}/configure --prefix=/usr --with-python=/usr/bin/python2
  make

  cd ../build-py3
  ../${pkgbase}-${pkgver}/configure --prefix=/usr
  make
}

package_python-gobject() {
  pkgdesc="Python 3 bindings for GObject"
  depends=('gobject-introspection' 'python-cairo' "pygobject-devel=$pkgver")

  cd build-py3
  make DESTDIR="$pkgdir" install
  rm -r "$pkgdir"/usr/{include,lib/pkgconfig}
}

package_python2-gobject() {
  pkgdesc="Python 2 bindings for GObject"
  depends=('gobject-introspection' 'python2-cairo' "pygobject-devel=$pkgver")

  cd build-py2
  make DESTDIR="$pkgdir" install
  mv "$pkgdir"/usr/{include,lib/pkgconfig} "$srcdir/devel"
}

package_pygobject-devel() {
  pkgdesc="Development files for the pygobject bindings"
  cd "devel"
  mkdir -p "$pkgdir"/usr/{include,lib}
  mv include "$pkgdir/usr/"
  mv pkgconfig "$pkgdir/usr/lib/"
}
