# $Id: PKGBUILD 204790 2014-01-27 08:48:48Z jgc $
# Maintainer: Jan de Groot <jgc@archlinux.org>
# Contributor: Tom Gundersen <teg@jklm.no>
# Contributor: Eduardo Romero <eduardo@archlinux.org>
# Contributor: Damir Perisa <damir.perisa@bluewin.ch>

pkgname=libgphoto2
pkgver=2.5.3.1
pkgrel=1
pkgdesc="The core library of gphoto2, designed to allow access to digital camera by external programs."
arch=(i686 x86_64 'mips64el')
url="http://www.gphoto.org"
license=(LGPL)
depends=('libexif' 'libjpeg' 'gd' 'libltdl' 'libusb')
install=libgphoto2.install
options=('libtool')
source=(http://downloads.sourceforge.net/gphoto/${pkgname}-${pkgver}.tar.bz2{,.asc})
md5sums=('aad2607a84442769c14f6acce2ca1ddf'
         'SKIP')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure \
	--prefix=/usr \
	--disable-rpath
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # Remove unused udev helper
  rm -rf "${pkgdir}/usr/lib/udev"

  install -m755 -d "${pkgdir}/usr/lib/udev/hwdb.d"
  LD_LIBRARY_PATH="${pkgdir}/usr/lib${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH" \
  CAMLIBS="${pkgdir}/usr/lib/libgphoto2/${pkgver}" \
      "${pkgdir}/usr/lib/libgphoto2/print-camera-list" hwdb > \
      "${pkgdir}/usr/lib/udev/hwdb.d/20-gphoto.conf"

  # Remove recursive symlink
  rm -f "${pkgdir}/usr/include/gphoto2/gphoto2"
}
