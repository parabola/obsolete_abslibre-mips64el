# $Id: PKGBUILD 204592 2014-01-23 13:42:13Z bpiotrowski $
# Maintainer:  Sergej Pupykin <pupykin.s+arch@gmail.com>
# Maintainer:  Bartłomiej Piotrowski <nospam@bpiotrowski.pl>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Tom Newsom <Jeepster@gmx.co.uk>
# Contributor: Paul Mattal <paul@archlinux.org>

pkgname=ffmpeg-compat
pkgver=0.10.11
pkgrel=1
epoch=1
pkgdesc='Complete and free Internet live audio and video broadcasting solution'
arch=('i686' 'x86_64' 'mips64el')
url='http://ffmpeg.org/'
license=('GPL')
depends=(
      'alsa-lib' 'bzip2' 'gsm' 'lame' 'libass'
      'libmodplug' 'libpulse' 'libtheora' 'libvorbis' 'libvpx'
      'opencore-amr' 'openjpeg' 'rtmpdump' 'schroedinger' 'sdl' 'speex'
      'v4l-utils' 'x264' 'xvidcore' 'zlib'
      )
if [ "$CARCH" != "mips64el" ]; then
  depends+=(libva)
  makedepends=('libvdpau' 'yasm')
fi
source=(http://ffmpeg.org/releases/ffmpeg-$pkgver.tar.bz2
        ffmpeg-0.10.10-freetype2.patch)
sha256sums=('99a81aa43325de9c670659c15496ef348b5c4c65306caa1ce0893ae5f4f636d0'
            '05cf516b292c3815ecaaf72f12c0c73891b62b47d83f5d81f09380949e691988')

prepare() {
  cd ffmpeg-$pkgver
  patch -p1 -i ../ffmpeg-0.10.10-freetype2.patch
}

build() {
  cd ffmpeg-$pkgver

  CFLAGS+=" -I/usr/include/openjpeg-1.5" \
  ./configure \
    --prefix=/usr \
    --incdir=/usr/include/ffmpeg-compat \
    --libdir=/usr/lib/ffmpeg-compat \
    --shlibdir=/usr/lib/ffmpeg-compat \
    --disable-debug \
    --disable-static \
    --enable-gpl \
    --enable-libass \
    --enable-libfreetype \
    --enable-libgsm \
    --enable-libmodplug \
    --enable-libmp3lame \
    --enable-libopencore_amrnb \
    --enable-libopencore_amrwb \
    --enable-libopenjpeg \
    --enable-libpulse \
    --enable-librtmp \
    --enable-libschroedinger \
    --enable-libspeex \
    --enable-libtheora \
    --enable-libv4l2 \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libx264 \
    --enable-libxvid \
    --enable-postproc \
    --enable-runtime-cpudetect \
    --enable-shared \
    --enable-vdpau \
    --enable-version3 \
    --enable-x11grab
 

  make
}

package() {
  cd ffmpeg-$pkgver
  make DESTDIR="$pkgdir" install
  rm -rf "$pkgdir"/usr/{bin,share}

  install -dm755 "$pkgdir"/etc/ld.so.conf.d/
  echo -e '/usr/lib/\n/usr/lib/ffmpeg-compat/' > "$pkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

# vim:set ts=2 sw=2 et:
