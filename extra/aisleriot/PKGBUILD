# $Id: PKGBUILD 199394 2013-11-11 22:17:10Z heftig $
# Maintainer: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>

pkgname=aisleriot
pkgver=3.10.2
pkgrel=1
pkgdesc="A collection of patience games written in guile scheme"
arch=(i686 x86_64 mips64el)
license=(GPL)
url="http://www.gnome.org"
groups=('gnome-extra')
depends=(guile gtk3 librsvg libcanberra gconf hicolor-icon-theme dconf)
makedepends=(intltool itstool docbook-xsl yelp-tools desktop-file-utils)
optdepends=('libkdegames: KDE card sets'
            'pysolfc: PySol card sets'
            'pysolfc-cardsets: PySol card sets')
options=('!emptydirs')
install=aisleriot.install
source=(http://ftp.gnome.org/pub/gnome/sources/$pkgname/${pkgver:0:4}/$pkgname-$pkgver.tar.xz)
sha256sums=('a2a31b2b450bcb5cca9b4b82b64877b784aff52e669e84e7b99fa731f0325082')

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
    --libexecdir=/usr/lib --disable-static \
    --with-card-theme-formats=all \
    --with-kde-card-theme-path=/usr/share/apps/carddecks \
    --with-pysol-card-theme-path=/usr/share/PySolFC


  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1
  install -m755 -d "$pkgdir/usr/share/gconf/schemas"
  gconf-merge-schema "$pkgdir/usr/share/gconf/schemas/$pkgname.schemas" \
    --domain aisleriot "$pkgdir"/etc/gconf/schemas/*.schemas
  rm -f "$pkgdir"/etc/gconf/schemas/*.schemas
}
