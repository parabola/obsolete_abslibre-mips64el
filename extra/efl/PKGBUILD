# $Id: PKGBUILD 205022 2014-02-03 10:02:52Z heftig $
# Maintainer: Ronald van Haren <ronald@archlinux.org>
# Contributor: Enlightenment Developers <enlightenment-devel@enlightenment.org>

pkgbase=efl
pkgname=('efl' 'efl-docs')
pkgver=1.8.5
pkgrel=2
pkgdesc="Enlightenment Foundation Libraries"
arch=('i686' 'x86_64')
url="http://www.enlightenment.org"
license=('BSD' 'LGPL2.1' 'GPL2' 'custom')
depends=('bullet' 'libjpeg-turbo' 'gstreamer0.10-base-plugins'
         'lua' 'curl' 'fribidi' 'libpulse' 'libxcomposite'
         'libxinerama' 'libxrandr' 'libxss' 'python2'
         'libxcursor' 'libxp' 'libwebp' 'shared-mime-info')
makedepends=('doxygen')
replaces=('ecore' 'edje' 'eet' 'eeze' 'efreet' 'eina' 'eio' 'embryo' 
	'emotion' 'ethumb' 'evas')
options=('!emptydirs')
source=(http://download.enlightenment.org/rel/libs/${pkgname}/$pkgname-$pkgver.tar.gz)
install=efl.install
sha1sums=('6ab88b263f784f935310b1055d67578ae66e50df')

prepare() {
  sed -i 's/env python$/&2/' "${srcdir}/${pkgname}-${pkgver}/src/scripts/eina/eina-bench-cmp"
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  export CFLAGS="$CFLAGS -fvisibility=hidden"

  ./configure --prefix=/usr \
  --disable-static --disable-tslib --enable-fb \
  --enable-xinput22 --enable-multisense --enable-systemd \
  --enable-image-loader-webp --enable-harfbuzz 

  make
  make -j1 doc
}

package_efl(){
  cd "${srcdir}/${pkgname}-${pkgver}"
  make -j1 DESTDIR=${pkgdir} install

  # install non-standard license files
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/licenses/COPYING.BSD" \
	"${pkgdir}/usr/share/licenses/${pkgname}/COPYING.BSD"
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/licenses/COPYING.SMALL" \
	"${pkgdir}/usr/share/licenses/${pkgname}/COPYING.SMALL"
}

package_efl-docs() {
  pkgdesc="Documentation for the Enlightenment Foundation Libraries"
  depends=()

  cd "${srcdir}/${pkgbase}-${pkgver}"
  install -d "${pkgdir}/usr/share/doc/${pkgbase}"
  cp -a doc/html "${pkgdir}/usr/share/doc/${pkgbase}/html"
  cp -a doc/latex "${pkgdir}/usr/share/doc/${pkgbase}/latex"
}

