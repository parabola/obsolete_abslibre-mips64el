# $Id: $
# Maintainer: Jan de Groot

pkgname=libevdev
pkgver=0.6
pkgrel=1
pkgdesc="Wrapper library for evdev devices"
arch=(i686 x86_64 mips64el)
url="http://www.freedesktop.org/wiki/Software/libevdev/"
license=(custom:X11)
depends=('glibc')
makedepends=('python')
source=(http://freedesktop.org/software/$pkgname/$pkgname-$pkgver.tar.xz
        0001-Revert-Drop-deprecated-functions.patch
        0001-Revert-Drop-some-leftover-deprecated-constants.patch)
sha256sums=('1c562ed32252ef9c14aa61e03b38acb886cb3edbbdafbb5181762ff16e010d93'
            '783ba5edf7c9525e8cac46c8c4c981c98395ff3c68ed7a162190ef9e8f13b149'
            '24d52811732b3335eb363416e4862c6152bfe6ebc8c372a298126221f754f041')

build() {
  cd $pkgname-$pkgver

  # Fix libevdev 0.6 missing symbol that breaks clutter, patches from Fedora
  # https://bugzilla.redhat.com/show_bug.cgi?id=1046426
  patch -Np1 -i ../0001-Revert-Drop-deprecated-functions.patch
  patch -Np1 -i ../0001-Revert-Drop-some-leftover-deprecated-constants.patch

  ./configure --prefix=/usr --disable-static --disable-gcov
  make
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
