# $Id: PKGBUILD 204740 2014-01-26 11:28:37Z andyrtr $
# Maintainer: Andreas Radke <andyrtr@archlinux.org>
# Contributor: Sergej Pupykin <sergej@aur.archlinux.org>

pkgname=man-db
pkgver=2.6.6
pkgrel=1
pkgdesc="A utility for reading man pages"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.nongnu.org/man-db/"
license=('GPL' 'LGPL')
groups=('base')
depends=( 'bash' 'gdbm' 'zlib' 'groff' 'libpipeline' 'less')
optdepends=('gzip')
backup=('etc/man_db.conf'
	    'etc/cron.daily/man-db')
conflicts=('man')
provides=('man')
replaces=('man')
install=${pkgname}.install
source=(http://savannah.nongnu.org/download/man-db/$pkgname-$pkgver.tar.xz{,.sig}
        convert-mans man-db.cron.daily)
md5sums=('5d65d66191080c144437a6c854e17868'
         'SKIP'
         '2b7662a7d5b33fe91f9f3e034361a2f6'
         '934fd047fecb915038bf4bf844ea609c')

build() {
  cd ${srcdir}/${pkgname}-${pkgver}
  ./configure --prefix=/usr \
    --sbindir=/usr/bin \
    --sysconfdir=/etc \
    --libexecdir=/usr/lib \
	--with-db=gdbm \
	--disable-setuid \
	--enable-mandirs=GNU \
	--with-sections="1 n l 8 3 0 2 5 4 9 6 7"
  make
}

check() {
  cd ${srcdir}/${pkgname}-${pkgver}
  make check
}

package() {
  cd ${srcdir}/${pkgname}-${pkgver}
  make DESTDIR=${pkgdir} install

  # part of groff pkg
  rm -f ${pkgdir}/usr/bin/zsoelim

  # script from LFS to convert manpages, see
  # http://www.linuxfromscratch.org/lfs/view/6.4/chapter06/man-db.html
  install -D -m755 ${srcdir}/convert-mans  ${pkgdir}/usr/bin/convert-mans 

  #install whatis cron script
  install -D -m744 ${srcdir}/man-db.cron.daily ${pkgdir}/etc/cron.daily/man-db
}

