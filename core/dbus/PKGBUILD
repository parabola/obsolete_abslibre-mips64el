# $Id: PKGBUILD 204675 2014-01-24 19:00:33Z dreisner $
# Maintainer: Tom Gundersen <teg@jklm.no>
# Maintainer: Jan de Groot <jgc@archlinux.org>
# Contributor: Link Dupont <link@subpop.net>
#
pkgbase=dbus
pkgname=('dbus' 'libdbus')
pkgver=1.8.0
pkgrel=1
pkgdesc="Freedesktop.org message bus system"
url="http://www.freedesktop.org/Software/dbus"
arch=(i686 x86_64 mips64el)
license=('GPL' 'custom')
makedepends=('libx11' 'systemd' 'xmlto' 'docbook-xsl')
source=(http://dbus.freedesktop.org/releases/dbus/dbus-$pkgver.tar.gz
        30-dbus)
md5sums=('059fbe84e39fc99c67a14f15b1f39dff'
         '3314d727fa57fc443fce25b5cbeebbcc')

prepare() {
  cd dbus-$pkgver
}

build() {
  cd dbus-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
      --libexecdir=/usr/lib/dbus-1.0 --with-dbus-user=dbus \
      --with-system-pid-file=/run/dbus/pid \
      --with-system-socket=/run/dbus/system_bus_socket \
      --with-console-auth-dir=/run/console/ \
      --enable-inotify --disable-dnotify \
      --disable-verbose-mode --disable-static \
      --disable-tests --disable-asserts \
      --with-systemdsystemunitdir=/usr/lib/systemd/system \
      --enable-systemd
  make
}

package_dbus(){
  depends=('libdbus' 'expat')
  optdepends=('libx11: dbus-launch support')
  provides=('dbus-core')
  conflicts=('dbus-core')
  replaces=('dbus-core')

  cd dbus-$pkgver
  make DESTDIR="$pkgdir" install

  rm -rf "$pkgdir/var/run"

  install -Dm755 ../30-dbus "$pkgdir/etc/X11/xinit/xinitrc.d/30-dbus"

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/dbus/COPYING"

  # split out libdbus-1
  rm -rf "$srcdir/_libdbus"
  install -dm755 "$srcdir"/_libdbus/usr/lib/dbus-1.0
  mv "$pkgdir"/usr/include "$srcdir"/_libdbus/usr/
  mv "$pkgdir"/usr/lib/pkgconfig "$srcdir"/_libdbus/usr/lib/
  mv "$pkgdir"/usr/lib/libdbus* "$srcdir"/_libdbus/usr/lib/
  mv "$pkgdir"/usr/lib/dbus-1.0/include "$srcdir"/_libdbus/usr/lib/dbus-1.0/
  install -Dm644 COPYING "$srcdir"/_libdbus/usr/share/licenses/libdbus/COPYING
}

package_libdbus(){
  pkgdesc="DBus library"
  depends=('glibc')


  mv "$srcdir"/_libdbus/* "$pkgdir"
}
