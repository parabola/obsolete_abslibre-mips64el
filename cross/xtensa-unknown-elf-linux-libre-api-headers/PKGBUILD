# $Id: PKGBUILD 180857 2013-03-27 11:11:40Z allan $
# Maintainer: Allan McRae <allan@archlinux.org>
# Maintainer (Parabola): André Silva <emulatorman@parabola.nu>

# toolchain build order: linux-libre-api-headers->glibc->binutils->gcc->binutils->glibc
export ARCH=xtensa
_target=xtensa-unknown-elf
_sysroot="/usr/${_target}"

pkgname=${_target}-linux-libre-api-headers
_basekernel=3.8
_sublevel=4
pkgver=${_basekernel}.${_sublevel}
pkgrel=1.3
pkgdesc="Kernel headers sanitized for use in userspace for the Xtensa architecture"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.gnu.org/software/libc"
license=('GPL2')
provides=("${_target}-linux-api-headers=${pkgver}")
conflicts=("${_target}-linux-api-headers")
replaces=("${_target}-linux-api-headers" )
source=("http://linux-libre.fsfla.org/pub/linux-libre/releases/${_basekernel}-gnu/linux-libre-${_basekernel}-gnu.tar.xz"
        "http://linux-libre.fsfla.org/pub/linux-libre/releases/${pkgver}-gnu/patch-${_basekernel}-gnu-${pkgver}-gnu.xz")
md5sums=('84c2a77910932ffc7d958744ac9cf2f5'
         'be610dd93dbe033cfe04018b27557c3e')

build() {
  cd ${srcdir}/linux-${_basekernel}

  if [ "${_basekernel}" != "${pkgver}" ]; then
    patch -Np1 -i "${srcdir}/patch-${_basekernel}-gnu-${pkgver}-gnu"
  fi

  make mrproper
  make headers_check
}

package() {
  cd ${srcdir}/linux-${_basekernel}
  make ARCH=${ARCH} INSTALL_HDR_PATH=${pkgdir} headers_install

  # use headers from libdrm
  rm -r ${pkgdir}/include/drm

  # clean-up unnecessary files generated during install
  find ${pkgdir} \( -name .install -o -name ..install.cmd \) -delete
}
