# Contributor: Nicolás Reynolds <fauno@parabola.nu>
# Contributor (mipsel): Vojtech Horky <vojta . horky at-symbol seznam . cz>
# This package is used to bootstrap a cross glibc and full featured cross gcc
# Change the following variables to match your target
_pkgname=gcc
_target="mips64el-unknown-linux-gnu"
_sysroot="/usr/${_target}"
_ARCH=mips

unset LDFLAGS CFLAGS CXXFLAGS

pkgname=cross-${_target}-gcc-core
pkgver=4.7.0
pkgrel=4.3
_snapshot=4.7-20120407
_libstdcppmanver=20120307		# Note: check source directory name when updating this
pkgdesc="The GNU Compiler Collection for the MIPS/Loongson2f architecture (static version)"
url="http://www.gnu.org/software/binutils/"
arch=('i686' 'x86_64')
license=('GPL')
depends=('libmpc' 'sh' "${_target}-binutils" 'cloog' 'ppl')
options=('!ccache' '!distcc' '!emptydirs' '!libtool' '!strip')
source=(#ftp://gcc.gnu.org/pub/gcc/releases/gcc-${pkgver}/gcc-${pkgver}.tar.bz2
	ftp://gcc.gnu.org/pub/gcc/snapshots/${_snapshot}/gcc-${_snapshot}.tar.bz2
	ftp://gcc.gnu.org/pub/gcc/libstdc++/doxygen/libstdc++-api.${_libstdcppmanver}.man.tar.bz2
	gcc_pure64.patch
    gcc_mips64el_lib.patch
	gcc-4.7.0-cloog-0.17.patch)
md5sums=('e44a8484687ef52c02066d6434ecf42b'
         '489d2f5311535800a120efd8d18db719'
         'ced48436c1b3c981d721a829f1094de1'
         '474b8f5a7dee6ddd96b95a7240bfb961'
         '575f7d17b022e609447a590e481b18b5')

if [ -n "${_snapshot}" ]; then
  _basedir="${srcdir}/gcc-${_snapshot}"
else
  _basedir="${srcdir}/gcc-${pkgver}"
fi

build() {
  cd ${_basedir}

  # Do not install libiberty
  sed -i 's/install_to_$(INSTALL_DEST) //' libiberty/Makefile.in

  # Do not run fixincludes
  sed -i 's@\./fixinc\.sh@-c true@' gcc/Makefile.in

  # compatibility with latest cloog
  patch -p1 -i ${srcdir}/gcc-4.7.0-cloog-0.17.patch

  echo ${pkgver} > gcc/BASE-VER

  cd ${srcdir}
  mkdir gcc-build && cd gcc-build

  #AS=${_target}-as \
  LDFLAGS= \
  CC_FOR_BUILD=${CHOST}-gcc \
  CFLAGS=" -pipe " \
  ${_basedir}/configure --prefix=/usr \
    --with-linker-hash-style=gnu \
    --build=${CHOST} --host=${CHOST} --target=${_target} \
    --with-sysroot=${_sysroot} --with-arch=loongson2f --with-abi=n32 \
    --with-libs --with-headers \
    --with-local-prefix=${_sysroot} \
    --disable-multilib \
    --disable-libmudflap \
    --with-newlib \
    --enable-threads=no \
    --disable-shared \
    --enable-__cxa_atexit \
    --with-gmp \
    --with-mpfr \
    --with-mpc \
    --with-ppl \
    --enable-cloog-backend=isl \
    --enable-lto \
    --enable-target-optspace \
    --disable-libgomp \
    --disable-libmudflap \
    --disable-nls \
    --enable-languages=c 

  make all-gcc

}

package() {
	cd ${srcdir}/${_pkgname}-build
	
	make ${MAKEFLAGS} DESTDIR=${pkgdir} install-gcc

    rm -r ${pkgdir}/usr/share
}
md5sums=('e44a8484687ef52c02066d6434ecf42b'
         '489d2f5311535800a120efd8d18db719'
         'ced48436c1b3c981d721a829f1094de1'
         '474b8f5a7dee6ddd96b95a7240bfb961'
         '575f7d17b022e609447a590e481b18b5')
