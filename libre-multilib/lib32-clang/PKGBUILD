# Maintainer: PitBall

pkgname=lib32-clang
pkgver=3.4
pkgrel=2
arch=('x86_64')
url="http://llvm.org/"
license=('custom:University of Illinois/NCSA Open Source License')
makedepends=('lib32-libffi' 'lib32-zlib' 'python2' 'gcc-multilib' 'clang' 'lib32-llvm' 'bc')
source=(http://llvm.org/releases/$pkgver/llvm-$pkgver.src.tar.gz
        http://llvm.org/releases/$pkgver/clang-$pkgver.src.tar.gz
        http://llvm.org/releases/$pkgver/compiler-rt-$pkgver.src.tar.gz)
sha256sums=('25a5612d692c48481b9b397e2b55f4870e447966d66c96d655241702d44a2628'
            '22a9780db3b85a7f2eb9ea1f7f6e00da0249e3d12851e8dea0f62f1783242b1b'
            'f37c89b1383ce462d47537a0245ac798600887a9be9f63073e16b79ed536ab5c')
options=('staticlibs')

prepare() {
  cd "$srcdir/llvm-$pkgver"

  rm -rf projects/compiler-rt
  mv "$srcdir/clang-$pkgver" tools/clang

  rm -rf projects/compiler-rt
  mv "$srcdir/compiler-rt-$pkgver" projects/compiler-rt

  # Fix installation directories, ./configure doesn't seem to set them right
  sed -i -e 's:\$(PROJ_prefix)/lib:$(PROJ_prefix)/lib32:' \
         -e 's:\$(PROJ_prefix)/docs/llvm:$(PROJ_prefix)/share/doc/llvm:' \
    Makefile.config.in
  sed -i '/ActiveLibDir = ActivePrefix/s:lib:lib32:' \
    tools/llvm-config/llvm-config.cpp
  sed -i 's:LLVM_LIBDIR="${prefix}/lib":LLVM_LIBDIR="${prefix}/lib32":' \
    autoconf/configure.ac \
    configure

  # Fix insecure rpath (http://bugs.archlinux.org/task/14017)
  sed -i 's:$(RPATH) -Wl,$(\(ToolDir\|LibDir\|ExmplDir\))::g' Makefile.rules

  # Use system lib32-llvm
  install -d $srcdir/llvm-$pkgver/Release/lib
  for file in ` pacman -Ql lib32-llvm |grep /lib32/ |awk '{print $2}' |sed '/\/$/d'`; do
  ln -sf $file $srcdir/llvm-$pkgver/Release/lib/
  done
}

build() {
  cd "$srcdir/llvm-$pkgver"
  export CC="gcc -m32"
  export CXX="g++ -m32"
  export CLANG="clang -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"
  export LLVM_CONFIG=/usr/bin/llvm-config32

  # Include location of libffi headers in CPPFLAGS
  CPPFLAGS+=" $(pkg-config --cflags libffi)"

  ./configure \
    --prefix=/usr \
    --libdir=/usr/lib32 \
    --sysconfdir=/etc \
    --enable-shared \
    --enable-libffi \
    --enable-targets=x86 \
    --enable-experimental-targets=R600 \
    --enable-bindings=none \
    --disable-expensive-checks \
    --disable-debug-runtime \
    --disable-assertions \
    --with-binutils-include=/usr/include \
    --with-python=/usr/bin/python2 \
    --host=i686-pc-linux-gnu \
    --target=i686-pc-linux-gnu \
    --enable-optimized

#    --disable-clang-static-analyzer \

#  make
  make -C projects/compiler-rt clang_linux
  make -C tools/clang/utils/TableGen
  make -C tools/clang/lib
}

package_lib32-clang() {
  pkgdesc="C language family frontend for LLVM (32-bit)"
  url="http://clang.llvm.org/"
  depends=('clang' 'lib32-llvm' 'gcc-multilib')

  install -d $pkgdir/usr/lib32/clang/$pkgver
  ln -s lib32 $pkgdir/usr/lib

  cd "$srcdir/llvm-$pkgver/tools/clang"
  make -C lib DESTDIR="$pkgdir" install
  # Fix permissions of static libs
  chmod -x "$pkgdir"/usr/lib32/*.a
  mv $pkgdir/usr/lib32/clang{,/$pkgver}/include
  rm -rf $pkgdir/usr/lib
}
