# $Id: PKGBUILD 105500 2014-02-09 11:01:04Z arodseth $
# Maintainer: Alexander Rødseth <rodseth@gmail.com>
# Contributor: Paulo Matias <matiasΘarchlinux-br·org>
# Contributor: Georgij Kondratjev <smpuj@bk.ru>
# Contributor: Daniel J Griffiths <ghost1227@archlinux.us>
# Maintainer (Parabola): André Silva <emulatorman@parabola.nu>

_pkgname=netsurf
pkgname=netsurf-libre
pkgver=3.0
pkgrel=7
pkgdesc='Lightweight and fast web browser, without non-privacy search providers'
arch=('x86_64' 'i686' 'mips64el')
url='http://www.netsurf-browser.org/'
license=('MIT' 'GPL2')
replaces=$_pkgname
conflicts=$_pkgname
provides=$_pkgname=$pkgver
depends=('gtk2' 'libmng' 'curl' 'librsvg' 'desktop-file-utils' 'libnsbmp' 'libnsgif' 'libcss' 'libwebp' 'libdom' 'lcms') # 'libharu' 'gstreamer0.10'
makedepends=('js' 're2c' 'gendesk' 'netsurf-buildsystem' 'libglade' 'addinclude')
install="$_pkgname.install"
source=("netsurf.png::http://ubuntu.allmyapps.com/data/n/e/netsurf-netsurf-web-browser/icon_48x48_netsurf.png"
        "http://download.$_pkgname-browser.org/$_pkgname/releases/source/$_pkgname-$pkgver-src.tar.gz"
        'netsurf.sh')
sha256sums=('f0dbcc5d80bf03d706aa8b28a322aa7f169a40813848c2d1505691f6e2c7ef00'
            '7c6a48d3cc3e9a3e3a51b532ddf60f7697e97bf8b61a6d3b2ced1a2e89fbccc6'
            '7aef20cc7b2e2e7225237f2f94b5a0074caeb8ec07075c58429b0345ca566f8d')

prepare() {
  cd "$_pkgname-$pkgver"

  # remove non-privacy search providers
  sed -i '\|Google|d
          \|Yahoo|d
          \|Bing|d
          \|Business.com|d
          \|Omgili|d
          \|BBC News|d
          \|Ubuntu Packages|d
          \|Ask[.]com|d
          \|Answers.com|d
          \|Youtube|d
          \|AeroMp3|d
          \|AOL|d
          \|Baidu|d
          \|Amazon|d
          \|Ebay|d
          \|IMDB|d
          \|ESPN|d
          \|Aminet|d
          \|OS4Depot|d
          s|DuckDuckGo|DuckDuckGo HTML|
          s|duckduckgo[.]com|duckduckgo.com/html|
          s|duckduckgo[.]com/html/favicon.ico|duckduckgo.com/favicon.ico|
          s|seeks-project.info|seeks.fr|g
         ' gtk/res/SearchEngines \
           \!NetSurf/Resources/SearchEngines \
           amiga/resources/SearchEngines
  sed -i '\|readable name such as| s|google|duckduckgo|
          \|readable name such as| s|yahoo|seeks|
          \|host address such as www[.]google[.]com| s|google[.]com|duckduckgo.com/html|
          s|google[.]com?search=%s|duckduckgo.com/html/?q=%s|
          \|return strdup| s|google[.]com|duckduckgo.com/html|
          \|return strdup| s|google|duckduckgo|
          \|return strdup| s|duckduckgo[.]com/html/favicon[.]ico|duckduckgo.com/favicon.ico|
          s|www[.]google[.]com/search?q=%s|duckduckgo.com/html/?q=%s|
         ' desktop/searchweb.c
  sed -i 's|google[.][.a-z]\{0,6\}/search|duckduckgo.com/html/search|
          s|Google-Suche|DuckDuckGo HTML|
          s|Google Search|DuckDuckGo HTML|
          s|Ricerca Google|DuckDuckGo HTML|
          s|Google検索|DuckDuckGo HTML|
          \|BBC News|d
          \|Reuters|d
          \|CNN|d
          \|Slashdot|d
          \|Ars Technica|d
          \|The Register|d
          \|W3C|d
          s|google[.][.a-z]\{0,6\}|duckduckgo.com/html|
          s|Google|DuckDuckGo HTML|
          s|[.a-z]\{0,4\}yahoo[.]co[.a-z]\{0,4\}|www.seeks.fr|
          s|Yahoo!|Seeks|
          \|IMDB|d
          s|The Icon Bar|Creative Commons|
          s|www[.]iconbar[.]com|creativecommons.org|
          s|ROOL|Dictionary.com|
          s|www[.]riscosopen[.]org|dictionary.reference.com|
          \|riscos.info|d
         ' \!NetSurf/Resources/de/welcome.html\,faf \
           \!NetSurf/Resources/en/welcome.html\,faf \
           \!NetSurf/Resources/it/welcome.html\,faf \
           \!NetSurf/Resources/ja/welcome.html\,faf

  addinclude image/rsvg.c stdio
  addinclude image/mng.c stdio
  addinclude image/png.c stdio

  gendesk -f --pkgname "$_pkgname" --pkgdesc "$pkgdesc" --exec 'netsurf %U' \
    --genericname 'Web Browser' --comment 'Lightweight web browser' \
    --mimetypes 'text/html;application/xhtml+xml;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/geo'
}

build() {
  cd "$_pkgname-$pkgver"

  make \
    PREFIX=/usr \
    TARGET=gtk \
    NETSURF_USE_WEBP=YES \
    NETSURF_USE_VIDEO=NO
}

package() {
  cd "$_pkgname-$pkgver"

  make install PREFIX=/usr DESTDIR="$pkgdir" NETSURF_USE_WEBP=YES
  mv "$pkgdir/usr/bin/$_pkgname" "$pkgdir/usr/bin/$_pkgname.elf"
  install -Dm755 "../$_pkgname.sh" "$pkgdir/usr/bin/$_pkgname"
  install -Dm644 "../$_pkgname.png" "$pkgdir/usr/share/pixmaps/$_pkgname.png"
  install -Dm644 "$_pkgname.desktop" \
    "$pkgdir/usr/share/applications/$_pkgname.desktop"
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$_pkgname/LICENSE"
}

# vim:set ts=2 sw=2 et:
