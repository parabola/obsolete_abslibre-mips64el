#!/usr/bin/make -f
# Assist file for ./compare

tmp/%.debian.filelist:
	curl http://packages.debian.org/sid/all/$*/filelist|sed -n "/<pre>/,/<\/pre>/{ s|.*<pre>||; s|</pre>.*||; /./p }"|sed 's|/||'|sort > $@

tmp/%.pacman.filelist:
	( cd pkg/$* && find * -not -type d; ) | sort > $@
