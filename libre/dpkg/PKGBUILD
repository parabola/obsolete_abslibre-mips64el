# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>
# Maintainer (AUR): Jochen Schalanda <jochen+aur@schalanda.name>
# Contributor (AUR): Pierre Carrier <pierre@spotify.com>
# Contributor (AUR): Thomas Dziedzic <gostrc@gmail>
# Contributor (AUR): Chris Giles <Chris.G.27@gmail.com>
# Contributor (AUR): seblu <seblu+arch@seblu.net>
# Contributor (AUR): squiddo <squiddo@intheocean.net>

pkgbase=dpkg
#pkgname=(dpkg dpkg-devtools dselect libdpkg perl-dpkg)
pkgname=(dpkg-devtools perl-dpkg)
pkgdesc="Debian Package manager and utilities"
license=('GPL2')

pkgver=1.17.6
url="http://packages.debian.org/source/sid/dpkg"

pkgrel=1
options=('staticlibs')

if ! type in_array &>/dev/null; then
	in_array() {
		local i
		for i in "${@:2}"; do [[ $i = "$1" ]] && return 0; done
		return 1
	}
fi

# The architecture dependent bits aren't packaged with the reduced pkgname
arch=('any')
for _pkg in dpkg dselect libdpkg; do
	if in_array "$_pkg" "${pkgname[@]}"; then
		arch=('i686' 'x86_64' 'mips64el')
	fi
done
unset _pkg

makedepends=('bzip2' 'xz' 'zlib' 'ncurses')
checkdepends=('gzip' 'perl-test-pod' 'perl-io-string' 'perl-timedate')

_debrepo=http://ftp.debian.org/debian/pool/main
_debfile() { echo "${_debrepo}/${1:0:1}/${1%_*}/${1}"; }
source=("$(_debfile ${pkgbase}_${pkgver}).tar.xz"
        dpkg-gzip-rsyncable.patch)
md5sums=('c87f2ae291d460015353e01d121bea85'
         '9c77a553e3219dcd29b7ff44b89d718d')

######################################################################

# This is really gross.
# It uses the debian control files to help us split the package.
# Shame on the dpkg developers for not putting that logic into the build system.
_destdir="$pkgbase-$pkgver/debian/pkg-makepkg"
_debhelper_install() {
	local debname=$1
	cd "$srcdir/$_destdir"

	# main files
	sed -e '/^\s*$/d' -e 's|usr/share/perl5/|&vendor_perl/|g' ../$debname.install |
	while read pattern dest; do
		if [[ -z $dest ]]; then
			for file in $pattern; do
				install -d "$pkgdir/${file%/*}"
				cp -a "$file" "$pkgdir/$file" || true
			done
		else
			install -d "$pkgdir/$dest/"
			cp -a "$pattern" "$pkgdir/$dest/" || true
		fi
	done

	# manpages
	sed -e 's|^debian/tmp/||' -e '/^\s*$/d' ../$debname.manpages 2>/dev/null |
	while read pattern; do
		for file in $pattern; do
			install -d "$pkgdir/${file%/*}"
			cp -a "$file" "$pkgdir/$file" || true
		done
	done

	# other documentation
	sed '/^\s*$/d' ../$debname.docs 2>/dev/null |
	while read file; do
		install -d "$pkgdir/usr/share/doc/$pkgname/"
		gzip \
			< "$srcdir/$pkgbase-$pkgver/$file" \
			> "$pkgdir/usr/share/doc/$pkgname/${file##*/}.gz"
	done

	# symlinks
	sed '/^\s*$/d' ../$debname.links 2>/dev/null |
	while read file link; do
		ln -s "/$file" "$pkgdir/$link"
	done
}

######################################################################

prepare() {
	cd "${srcdir}/${pkgbase}-${pkgver}"
	# {Arch Linux,Parabola}'s gzip doesn't support --rsyncable
	patch -Np1 -i "${srcdir}/dpkg-gzip-rsyncable.patch"
}

build() {
	cd "${srcdir}/${pkgbase}-${pkgver}"
	./configure \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--sysconfdir=/etc \
		--sbindir=/sbin \
		--localstatedir=/var \
		--with-zlib \
		--with-liblzma \
		--with-bz2
	make
	make DESTDIR="$srcdir/$_destdir" install
}

check() {
	cd "${srcdir}/${pkgbase}-${pkgver}"
	make check
}

package_dpkg() {
	pkgdesc="Debian Package manager"
	depends=('zlib' 'xz' 'bzip2')
	_debhelper_install dpkg
}

package_dpkg-devtools() {
	pkgdesc="Debian Package development tools"
	arch=('any')
	depends=('perl-dpkg')
	_debhelper_install dpkg-dev
}

package_dselect() {
	pkgdesc="Debian Package manager high-level interface"
	depends=('dpkg' 'perl' 'ncurses')
	_debhelper_install dselect
}

package_libdpkg() {
	pkgdesc="Debian Package manager library (static)"
	_debhelper_install libdpkg-dev
}

package_perl-dpkg() {
	pkgdesc="Debian Package Perl modules"
	arch=('any')
	depends=('perl-timedate' 'gzip' 'bzip2' 'xz')
	_debhelper_install libdpkg-perl
}
