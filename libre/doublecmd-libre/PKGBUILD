# vim:set ft=sh:
# $Id: PKGBUILD 103163 2013-12-28 11:01:37Z idevolder $
# Maintainer: BlackIkeEagle <ike DOT devolder AT gmail DOT com>
# Contributor: (sirocco AT ngs.ru)

_pkgbase=doublecmd
pkgbase=doublecmd-libre
pkgname=('doublecmd-libre-gtk2' 'doublecmd-libre-qt')
pkgver=0.5.8
_helpver=0.5.5
pkgrel=1.1
url="http://doublecmd.sourceforge.net/"
arch=('i686' 'x86_64')
license=('GPL')
install="$_pkgbase.install"
provides=("$_pkgbase")
makedepends=('lazarus' 'qt4pas' 'gtk2')
optdepends=(
	'lua51: scripting'
	'p7zip: support for 7zip archives'
	'unar: support for rar archives'
)
source=(
	"http://downloads.sourceforge.net/project/$_pkgbase/Double%20Commander%20Source/$_pkgbase-$pkgver-src.tar.gz"
	"http://downloads.sourceforge.net/project/$_pkgbase/Double%20Commander%20Source/$_pkgbase-help-$_helpver-src.tar.gz"
)

build() {
	cp -a $_pkgbase-$pkgver $_pkgbase-gtk
	cp -a $_pkgbase-$pkgver $_pkgbase-qt

	cd "$srcdir/$_pkgbase-gtk"
	if [ "$CARCH" = "i686" ]; then
		sed -e '/fPIC/d' -i "$srcdir/$_pkgbase-gtk/components/doublecmd/doublecmd_common.lpk"
	fi
	sed -e 's/\(export\ lazbuild=\).*/\1"$(which\ lazbuild) --lazarusdir=\/usr\/lib\/lazarus"/' -i build.sh
	./build.sh beta gtk2

	cd "$srcdir/$_pkgbase-qt"
	# dont use fPIC on i686
	if [ "$CARCH" = "i686" ]; then
		sed -e '/fPIC/d' -i "$srcdir/$_pkgbase-qt/components/doublecmd/doublecmd_common.lpk"
	fi
	sed -e 's/\(export\ lazbuild=\).*/\1"$(which\ lazbuild) --lazarusdir=\/usr\/lib\/lazarus"/' -i build.sh
	./build.sh beta qt
}

package_doublecmd-libre-gtk2() {
	pkgdesc="twin-panel (commander-style) file manager (GTK), with unar recommendation"
	depends=('gtk2')
	conflicts=('doublecmd-libre-qt' 'doublecmd-gtk2' 'doublecmd-gtk2-libre')
	replaces=('doublecmd-gtk2' 'doublecmd-gtk2-libre')
	provides=("doublecmd-gtk2=${pkgver}" 'doublecmd-gtk2-libre')
	cd "$srcdir/$_pkgbase-gtk"
	sed -e 's/LIB_SUFFIX=.*/LIB_SUFFIX=/g' -i ./install/linux/install.sh
	./install/linux/install.sh --install-prefix="$pkgdir"

	# install doc
	cd "$srcdir/$_pkgbase-help-$_helpver"
	cp -a * "$pkgdir/usr/share/$_pkgbase/doc/"
}

package_doublecmd-libre-qt() {
	pkgdesc="twin-panel (commander-style) file manager (QT), with unar recommendation"
	depends=('qt4pas')
	conflicts=('doublecmd-libre-gtk2' 'doublecmd-qt' 'doublecmd-qt-libre')
	replaces=('doublecmd-qt' 'doublecmd-qt-libre')
	provides=("doublecmd-qt=${pkgver}" 'doublecmd-qt-libre')
	cd "$srcdir/$_pkgbase-qt"
	sed -e 's/LIB_SUFFIX=.*/LIB_SUFFIX=/g' -i ./install/linux/install.sh
	./install/linux/install.sh --install-prefix="$pkgdir"

	# install doc
	cd "$srcdir/$_pkgbase-help-$_helpver"
	cp -a * "$pkgdir/usr/share/$_pkgbase/doc/"
}

sha256sums=('bfa85693b6cc06b7fd28ec8bd443ad9fb9d79d27a541e4f4d54bb9da2fb052ea'
            '5c5d00187df811df0734bf751a581bce7e1bdd4cf4639b2a1101f1da8743daaf')
