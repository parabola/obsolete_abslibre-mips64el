# $Id$
# Maintainer: Lukas Jirkovsky <l.jirkovsky@gmail.com>
# Contributor: flixie <69one@gmx.net>
# Contributor: Imanol Celaya <ornitorrincos@archlinux-es.org>
# Maintainer (Parabola): Márcio Silva <coadde@lavabit.com>
_pkgname=luxrender
pkgname=$_pkgname-libre
pkgver=1.3.1
_pkgver=d0b0e20c47cc
pkgrel=3
pkgdesc="Rendering system for physically correct, unbiased image synthesis, without nonfree OpenCL recommendation"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.$_pkgname.net/"
license=('GPL')
depends=('boost-libs' 'freeimage' 'openexr' 'mesa-libcl' 'mesa-libgl' 'fftw')
optdepends=('blender-addon-luxrender: Blender for renderer and exporter' 'qt4: Qt GUI' \
            'python: Python interface (pylux)')
makedepends=('cmake' 'boost' 'mesa' 'qt4' "luxrays=$pkgver" 'python' 'opencl-headers' \
             'clang')
provides=($_pkgname=$pkgver)
conflicts=$_pkgname
replaces=$_pkgname
source=(https://bitbucket.org/$_pkgname/lux/get/$_pkgver.tar.bz2 \
        force_python3.diff)
md5sums=('cbe749f56a1e1976745f5458100efa8a'
         '42692e65eabc5828693e2682e94b7c64')

prepare() {
  cd "$srcdir"/$_pkgname-lux-$_pkgver

  patch -Np1 < "$srcdir/force_python3.diff" || true
}

build() {
  cd "$srcdir"/$_pkgname-lux-$_pkgver

  export CC=clang
  export CXX=clang++
  export CXXFLAGS="$CXXFLAGS -lpthread"
  cmake . -DCMAKE_INSTALL_PREFIX=/usr \
    -DLUXRAYS_DISABLE_OPENCL=OFF \
    -DPYTHON_CUSTOM=ON \
    -DPYTHON_LIBRARIES=/usr/lib/libpython3.3m.so \
    -DPYTHON_INCLUDE_PATH=/usr/include/python3.3m
  make
}

package() {
  cd "$srcdir"/$_pkgname-lux-$_pkgver
  make DESTDIR="$pkgdir" install

  # fix library path on 64bit
  [[ $CARCH == x86_64 ]] && mv "$pkgdir"/usr/lib64 "$pkgdir"/usr/lib

  #install pylux
  install -D -m644 pylux.so "$pkgdir"/usr/lib/python3.3/pylux.so
}

# vim:set ts=2 sw=2 et:
