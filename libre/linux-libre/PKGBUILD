# $Id: PKGBUILD 206177 2014-02-20 22:43:41Z thomas $
# Maintainer: Tobias Powalowski <tpowa@archlinux.org>
# Maintainer: Thomas Baechler <thomas@archlinux.org>
# Maintainer (Parabola): André Silva <emulatorman@parabola.nu>
# Contributor (Parabola): Nicolás Reynolds <fauno@kiwwwi.com.ar>
# Contributor (Parabola): Sorin-Mihai Vârgolici <smv@yobicore.org>
# Contributor (Parabola): Michał Masłowski <mtjm@mtjm.eu>
# Contributor (Parabola): Márcio Silva <coadde@parabola.nu>

pkgbase=linux-libre         # Build stock -LIBRE kernel
#pkgbase=linux-libre-custom # Build kernel with a different name
_basekernel=3.13
_sublevel=4
pkgver=${_basekernel}.${_sublevel}
pkgrel=1
_lxopkgver=${_basekernel}.3 # nearly always the same as pkgver
arch=('i686' 'x86_64' 'mips64el')
url="http://linux-libre.fsfla.org/"
license=('GPL2')
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc')
options=('!strip')
source=("http://linux-libre.fsfla.org/pub/linux-libre/releases/${_basekernel}-gnu/linux-libre-${_basekernel}-gnu.tar.xz"
        "http://linux-libre.fsfla.org/pub/linux-libre/releases/${pkgver}-gnu/patch-${_basekernel}-gnu-${pkgver}-gnu.xz"
        # the main kernel config files
        'config.i686' 'config.x86_64'
        # standard config files for mkinitcpio ramdisk
        "${pkgbase}.preset"
        'Kbuild'
        'Kbuild.platforms'
        'boot-logo.patch'
        'change-default-console-loglevel.patch'
        'criu-no-expert.patch'
        '0001-sunrpc-create-a-new-dummy-pipe-for-gssd-to-hold-open.patch'
        '0002-sunrpc-replace-sunrpc_net-gssd_running-flag-with-a-m.patch'
        '0003-nfs-check-if-gssd-is-running-before-attempting-to-us.patch'
        '0004-rpc_pipe-remove-the-clntXX-dir-if-creating-the-pipe-.patch'
        '0005-sunrpc-add-an-info-file-for-the-dummy-gssd-pipe.patch'
        '0006-rpc_pipe-fix-cleanup-of-dummy-gssd-directory-when-no.patch'
        '0001-SUNRPC-Ensure-that-gss_auth-isn-t-freed-before-its-u.patch'
        '0001-syscalls.h-use-gcc-alias-instead-of-assembler-aliase.patch'
        '0001-quirk-asm_volatile_goto.patch'
        'i8042-fix-aliases.patch'
        "http://www.linux-libre.fsfla.org/pub/linux-libre/lemote/gnewsense/pool/debuginfo/linux-patches-${_lxopkgver}-gnu_0loongsonlibre_mipsel.tar.xz")
md5sums=('98a8e803e0ed08557f3cdd4d56b0ddc1'
         '3659d30b1d06dd5b7874ae04c946863b'
         'b6a3a3f9cac1be38384241ad58d45d46'
         '3740951ae165b89a2139d45ae7d82173'
         'e49ac236dfeef709f91a3d993ea7b62c'
         '2967cecc3af9f954ccc822fd63dca6ff'
         '8267264d9a8966e57fdacd1fa1fc65c4'
         '44260d2cb1a8b51c119d2ce1f83e457a'
         '98beb36f9b8cf16e58de2483ea9985e3'
         '989dc54ff8b179b0f80333cc97c0d43f'
         'dd2adb99cd3feed6f11022562901965c'
         'b00cc399d3797cb0793e18b5bf387a50'
         '7cbd2349cdf046acc37b652c06ba36be'
         '10dbaf863e22b2437e68f9190d65c861'
         'd5907a721b97299f0685c583499f7820'
         'a724515b350b29c53f20e631c6cf9a14'
         '1ae4ec847f41fa1b6d488f956e94c893'
         'e6fa278c092ad83780e2dd0568e24ca6'
         '6baa312bc166681f48e972824f3f6649'
         '93dbf73af819b77f03453a9c6de2bb47'
         'ac92b702b8497d2be14f96e077a7f48f')
if [ "$CARCH" != "mips64el" ]; then
    # don't use the Loongson-specific patches on non-mips64el arches.
    unset source[${#source[@]}-1]
    unset md5sums[${#md5sums[@]}-1]
fi

_kernelname=${pkgbase#linux-libre}
_localversionname=-LIBRE

prepare() {
  cd "${srcdir}/linux-${_basekernel}"

  if [ "${_basekernel}" != "${pkgver}" ]; then
    patch -p1 -i "${srcdir}/patch-${_basekernel}-gnu-${pkgver}-gnu"
  fi

  # add freedo as boot logo
  patch -p1 -i "${srcdir}/boot-logo.patch"

  # add latest fixes from stable queue, if needed
  # http://git.kernel.org/?p=linux/kernel/git/stable/stable-queue.git

  # set DEFAULT_CONSOLE_LOGLEVEL to 4 (same value as the 'quiet' kernel param)
  # remove this when a Kconfig knob is made available by upstream
  # (relevant patch sent upstream: https://lkml.org/lkml/2011/7/26/227)
  patch -p1 -i "${srcdir}/change-default-console-loglevel.patch"

  # allow Checkpoint/restore (for criu) without EXPERT=y
  patch -p1 -i "${srcdir}/criu-no-expert.patch"

  # fix 15 seconds nfs delay
  # http://git.linux-nfs.org/?p=trondmy/linux-nfs.git;a=commitdiff;h=4b9a445e3eeb8bd9278b1ae51c1b3a651e370cd6
  patch -p1 -i "${srcdir}/0001-sunrpc-create-a-new-dummy-pipe-for-gssd-to-hold-open.patch"
  # http://git.linux-nfs.org/?p=trondmy/linux-nfs.git;a=commitdiff;h=89f842435c630f8426f414e6030bc2ffea0d6f81
  patch -p1 -i "${srcdir}/0002-sunrpc-replace-sunrpc_net-gssd_running-flag-with-a-m.patch"
  # http://git.linux-nfs.org/?p=trondmy/linux-nfs.git;a=commitdiff;h=6aa23d76a7b549521a03b63b6d5b7880ea87eab7
  patch -p1 -i "${srcdir}/0003-nfs-check-if-gssd-is-running-before-attempting-to-us.patch"

  # fix nfs kernel oops
  # http://git.linux-nfs.org/?p=trondmy/linux-nfs.git;a=commitdiff;h=3396f92f8be606ea485b0a82d4e7749a448b013b
  patch -p1 -i "${srcdir}/0004-rpc_pipe-remove-the-clntXX-dir-if-creating-the-pipe-.patch"
  # http://git.linux-nfs.org/?p=trondmy/linux-nfs.git;a=commitdiff;h=e2f0c83a9de331d9352185ca3642616c13127539
  patch -p1 -i "${srcdir}/0005-sunrpc-add-an-info-file-for-the-dummy-gssd-pipe.patch"
  # http://git.linux-nfs.org/?p=trondmy/linux-nfs.git;a=commitdiff;h=23e66ba97127ff3b064d4c6c5138aa34eafc492f
  patch -p1 -i "${srcdir}/0006-rpc_pipe-fix-cleanup-of-dummy-gssd-directory-when-no.patch"

  # Fix FS#38921
  # http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=9eb2ddb48ce3a7bd745c14a933112994647fa3cd
  patch -p1 -i "${srcdir}/0001-SUNRPC-Ensure-that-gss_auth-isn-t-freed-before-its-u.patch"

  # Fix symbols: Revert http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=83460ec8dcac14142e7860a01fa59c267ac4657c
  patch -Rp1 -i "${srcdir}/0001-syscalls.h-use-gcc-alias-instead-of-assembler-aliase.patch"

  # Fix i8042 aliases
  patch -p1 -i "${srcdir}/i8042-fix-aliases.patch"
  # Fix compile issues
  # http://git.kernel.org/cgit/linux/kernel/git/tip/tip.git/patch/?id=a9f180345f5378ac87d80ed0bea55ba421d83859
  patch -Np1 -i "${srcdir}/0001-quirk-asm_volatile_goto.patch"

  if [ "$CARCH" == "mips64el" ]; then
    sed -i "s|^EXTRAVERSION.*|EXTRAVERSION =-libre|" Makefile
    sed -r "s|^( SUBLEVEL = ).*|\1$_sublevel|" \
        < "${srcdir}/config.patch" > config.patch
    msg2 "Adding loongson-community patches"
    patch -p1 -i ${srcdir}/${_basekernel}*-*-loongson-community.patch
    patch -p0 -i lxo-config.patch

    # ensure N32, add localversion, remove uevent helper as per
    # https://git.kernel.org/?p=linux/hotplug/udev.git;a=blob_plain;f=README
    # and make USB storage support builtin (e.g. for booting from USB
    # disks without slowly loading an initramfs)
    # since 3.4 rtl8187 supports adhoc mode, so batman-adv is enabled again
    sed -ri \
        -e "s|CONFIG_MIPS32_N32=.*|CONFIG_MIPS32_N32=y|g" \
        -e "s|CONFIG_UEVENT_HELPER_PATH=.*|CONFIG_UEVENT_HELPER_PATH=\"\"|g" \
        -e "s|CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\"${_localversionname}\"|g" \
        -e "s|# CONFIG_BATMAN_ADV is not set|CONFIG_BATMAN_ADV=m|g" \
        ./.config

    for i in BATMAN_ADV_BLA BATMAN_ADV_DAT BATMAN_ADV_NC; do
      echo "CONFIG_${i}=y" >>.config
    done

    for i in ATH9K_LEGACY_RATE_CONTROL BATMAN_ADV_DEBUG; do
      echo "# CONFIG_${i} is not set" >>.config
    done
  else
    cat "${srcdir}/config.${CARCH}" > ./.config # simpler
  fi

  if [ "${_kernelname}" != "" ]; then
    sed -i "s|CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\"${_localversionname}\"|g" ./.config
    sed -i "s|CONFIG_LOCALVERSION_AUTO=.*|CONFIG_LOCALVERSION_AUTO=n|" ./.config
  fi

  # set extraversion to pkgrel
  sed -ri "s|^(EXTRAVERSION =).*|\1 -${pkgrel}|" Makefile

  # don't run depmod on 'make install'. We'll do this ourselves in packaging
  sed -i '2iexit 0' scripts/depmod.sh

  cd "${srcdir}/linux-${_basekernel}"

  # get kernel version
  make prepare

  # load configuration
  # Configure the kernel. Replace the line below with one of your choice.
  #make menuconfig # CLI menu for configuration
  #make nconfig # new CLI menu for configuration
  #make xconfig # X-based configuration
  #make oldconfig # using old config from previous kernel version
  # ... or manually edit .config

  # rewrite configuration
  yes "" | make config >/dev/null
}

build() {
  cd "${srcdir}/linux-${_basekernel}"

  if [ "$CARCH" == "mips64el" ]; then
    # The build system passes it directly to linker, disable to avoid
    # having unknown -Wl,... options.
    export LDFLAGS=""
    # bzImage is arch-specific and not supported on mips; vmlinux is
    # useful for oprofile.
    make ${MAKEFLAGS} LOCALVERSION= vmlinux vmlinuz modules
  else
    make ${MAKEFLAGS} LOCALVERSION= bzImage modules
  fi
}

_package() {
  pkgdesc="The ${pkgbase^} kernel and modules"
  [ "${pkgbase}" = "linux-libre" ] && groups=('base')
  depends=('coreutils' 'linux-libre-firmware' 'kmod')
  optdepends=('crda: to set the correct wireless channels of your country')
  provides=("kernel26${_kernelname}=${pkgver}" "linux${_kernelname}=${pkgver}")
  conflicts=("kernel26${_kernelname}" "kernel26-libre${_kernelname}" "linux${_kernelname}")
  replaces=("kernel26${_kernelname}" "kernel26-libre${_kernelname}" "linux${_kernelname}")
  backup=("etc/mkinitcpio.d/${pkgbase}.preset")
  install=${pkgbase}.install
  if [ "$CARCH" != "mips64el" ]; then
     provides+=("linux-libre${_kernelname}-kmod-alx")
     conflicts+=("linux-libre${_kernelname}-kmod-alx")
     replaces+=("linux-libre${_kernelname}-kmod-alx")
  fi
  if [ "$CARCH" = "mips64el" ]; then
      optdepends+=('mkinitcpio: to make the initramfs (needs reinstall of this package)')
      conflicts+=('mkinitcpio<0.7')
  else
      depends+=('mkinitcpio>=0.7')
  fi

  cd "${srcdir}/linux-${_basekernel}"

  KARCH=x86
  [ $CARCH = "mips64el" ] && KARCH=mips

  # get kernel version
  _kernver="$(make LOCALVERSION= kernelrelease)"
  _basekernel=${_kernver%%-*}
  _basekernel=${_basekernel%.*}

  mkdir -p "${pkgdir}"/{lib/modules,lib/firmware,boot}
  make LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}" modules_install

  if [ "$CARCH" == "mips64el" ]; then
    cp vmlinuz "${pkgdir}/boot/vmlinuz-${pkgbase}"
    cp vmlinux "${pkgdir}/boot/vmlinux-${pkgbase}"
  else
    cp arch/${KARCH}/boot/bzImage "${pkgdir}/boot/vmlinuz-${pkgbase}"
  fi

  # set correct depmod command for install
  cp -f "${startdir}/${install}" "${startdir}/${install}.pkg"
  true && install=${install}.pkg
  sed \
    -e  "s/KERNEL_NAME=.*/KERNEL_NAME=${_kernelname}/" \
    -e  "s/KERNEL_VERSION=.*/KERNEL_VERSION=${_kernver}/" \
    -i "${startdir}/${install}"

  # install mkinitcpio preset file for kernel
  install -D -m644 "${srcdir}/${pkgbase}.preset" "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"
  sed \
    -e "1s|'linux*.*'|'${pkgbase}'|" \
    -e "s|ALL_kver=.*|ALL_kver=\"/boot/vmlinuz-${pkgbase}\"|" \
    -e "s|default_image=.*|default_image=\"/boot/initramfs-${pkgbase}.img\"|" \
    -e "s|fallback_image=.*|fallback_image=\"/boot/initramfs-${pkgbase}-fallback.img\"|" \
    -i "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"

  # mkinitcpio 0.7 relies on bzImage to find the kernel version
  if [ "$CARCH" == "mips64el" ]; then
    sed -e "s|ALL_kver=.*|ALL_kver=\"${_kernver}\"|" \
        -i "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"
  fi

  # remove build and source links
  rm -f "${pkgdir}"/lib/modules/${_kernver}/{source,build}
  # remove the firmware
  rm -rf "${pkgdir}/lib/firmware"
  # gzip -9 all modules to save 100MB of space
  find "${pkgdir}" -name '*.ko' -exec gzip -9 {} \;
  # make room for external modules
  ln -s "../extramodules-${_basekernel}${_localversionname:--LIBRE}" "${pkgdir}/lib/modules/${_kernver}/extramodules"
  # add real version for building modules and running depmod from post_install/upgrade
  mkdir -p "${pkgdir}/lib/modules/extramodules-${_basekernel}${_localversionname:--LIBRE}"
  echo "${_kernver}" > "${pkgdir}/lib/modules/extramodules-${_basekernel}${_localversionname:--LIBRE}/version"

  # Now we call depmod...
  depmod -b "${pkgdir}" -F System.map "${_kernver}"

  # move module tree /lib -> /usr/lib
  mkdir -p "${pkgdir}/usr"
  mv "${pkgdir}/lib" "${pkgdir}/usr/"

  # add vmlinux
  install -D -m644 vmlinux "${pkgdir}/usr/lib/modules/${_kernver}/build/vmlinux" 
}

_package-headers() {
  pkgdesc="Header files and scripts for building modules for ${pkgbase^} kernel"
  provides=("kernel26${_kernelname}-headers=${pkgver}" "linux${_kernelname}-headers=${pkgver}")
  conflicts=("kernel26${_kernelname}-headers" "kernel26-libre${_kernelname}-headers" "linux${_kernelname}-headers")
  replaces=("kernel26${_kernelname}-headers" "kernel26-libre${_kernelname}-headers" "linux${_kernelname}-headers")

  KARCH=x86
  [ $CARCH = "mips64el" ] && KARCH=mips

  # in case of repackaging this is empty
  if [ -z "${_kernver}" ]; then
    cd "${srcdir}/linux-${_basekernel}"
    _kernver="$(make LOCALVERSION= kernelrelease)"
  fi

  install -dm755 "${pkgdir}/usr/lib/modules/${_kernver}"

  cd "${srcdir}/linux-${_basekernel}"
  install -D -m644 Makefile \
    "${pkgdir}/usr/lib/modules/${_kernver}/build/Makefile"
  install -D -m644 kernel/Makefile \
    "${pkgdir}/usr/lib/modules/${_kernver}/build/kernel/Makefile"
  install -D -m644 .config \
    "${pkgdir}/usr/lib/modules/${_kernver}/build/.config"

  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/include"

  for i in acpi asm-generic config crypto drm generated keys linux math-emu \
    media net pcmcia scsi sound trace uapi video xen; do
    cp -a include/${i} "${pkgdir}/usr/lib/modules/${_kernver}/build/include/"
  done

  # copy arch includes for external modules
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}"
  cp -a arch/${KARCH}/include "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/"

  # copy files necessary for later builds
  cp Module.symvers "${pkgdir}/usr/lib/modules/${_kernver}/build"
  cp -a scripts "${pkgdir}/usr/lib/modules/${_kernver}/build"

  if [ "$CARCH" = "mips64el" ]; then
    cp arch/${KARCH}/Kbuild "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/"
    cp -a arch/${KARCH}/loongson "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/"
    cp ${srcdir}/Kbuild.platforms "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/"
  fi

  # fix permissions on scripts dir
  chmod og-w -R "${pkgdir}/usr/lib/modules/${_kernver}/build/scripts"
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/.tmp_versions"

  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/kernel"

  cp arch/${KARCH}/Makefile "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/"

  if [ "${CARCH}" = "i686" ]; then
    cp arch/${KARCH}/Makefile_32.cpu "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/"
  fi

  cp arch/${KARCH}/kernel/asm-offsets.s "${pkgdir}/usr/lib/modules/${_kernver}/build/arch/${KARCH}/kernel/"

  # add headers for lirc package
  # pci
  for i in bt8xx cx88 saa7134; do
    mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/pci/${i}"
    cp -a drivers/media/pci/${i}/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/pci/${i}"
  done
  # usb
  for i in cpia2 em28xx pwc sn9c102; do
    mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/usb/${i}"
    cp -a drivers/media/usb/${i}/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/usb/${i}"
  done
  # i2c
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/i2c"
  cp drivers/media/i2c/*.h  "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/i2c/"
  for i in cx25840; do
    mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/i2c/${i}"
    cp -a drivers/media/i2c/${i}/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/i2c/${i}"
  done

  # add docbook makefile
  install -D -m644 Documentation/DocBook/Makefile \
    "${pkgdir}/usr/lib/modules/${_kernver}/build/Documentation/DocBook/Makefile"

  # add dm headers
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/md"
  cp drivers/md/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/md"

  # add inotify.h
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/include/linux"
  cp include/linux/inotify.h "${pkgdir}/usr/lib/modules/${_kernver}/build/include/linux/"

  # add wireless headers
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/net/mac80211/"
  cp net/mac80211/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/net/mac80211/"

  # add dvb headers for external modules
  # in reference to:
  # http://bugs.archlinux.org/task/9912
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/dvb-core"
  cp drivers/media/dvb-core/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/dvb-core/"
  # and...
  # http://bugs.archlinux.org/task/11194
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/include/config/dvb/"
  cp include/config/dvb/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/include/config/dvb/"

  # add dvb headers for http://mcentral.de/hg/~mrec/em28xx-new
  # in reference to:
  # http://bugs.archlinux.org/task/13146
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/dvb-frontends/"
  cp drivers/media/dvb-frontends/lgdt330x.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/dvb-frontends/"
  cp drivers/media/i2c/msp3400-driver.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/i2c/"

  # add dvb headers
  # in reference to:
  # http://bugs.archlinux.org/task/20402
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/usb/dvb-usb"
  cp drivers/media/usb/dvb-usb/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/usb/dvb-usb/"
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/dvb-frontends"
  cp drivers/media/dvb-frontends/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/dvb-frontends/"
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/tuners"
  cp drivers/media/tuners/*.h "${pkgdir}/usr/lib/modules/${_kernver}/build/drivers/media/tuners/"

  # add xfs and shmem for aufs building
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/fs/xfs"
  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build/mm"
  cp fs/xfs/xfs_sb.h "${pkgdir}/usr/lib/modules/${_kernver}/build/fs/xfs/xfs_sb.h"

  # copy in Kconfig files
  for i in $(find . -name "Kconfig*"); do
    mkdir -p "${pkgdir}"/usr/lib/modules/${_kernver}/build/`echo ${i} | sed 's|/Kconfig.*||'`
    cp ${i} "${pkgdir}/usr/lib/modules/${_kernver}/build/${i}"
  done

  chown -R root.root "${pkgdir}/usr/lib/modules/${_kernver}/build"
  find "${pkgdir}/usr/lib/modules/${_kernver}/build" -type d -exec chmod 755 {} \;

  # strip scripts directory
  find "${pkgdir}/usr/lib/modules/${_kernver}/build/scripts" -type f -perm -u+w 2>/dev/null | while read binary ; do
    case "$(file -bi "${binary}")" in
      *application/x-sharedlib*) # Libraries (.so)
        /usr/bin/strip ${STRIP_SHARED} "${binary}";;
      *application/x-archive*) # Libraries (.a)
        /usr/bin/strip ${STRIP_STATIC} "${binary}";;
      *application/x-executable*) # Binaries
        /usr/bin/strip ${STRIP_BINARIES} "${binary}";;
    esac
  done

  # remove unneeded architectures
  rm -rf "${pkgdir}"/usr/lib/modules/${_kernver}/build/arch/{alpha,arc,arm,arm26,arm64,avr32,blackfin,c6x,cris,frv,h8300,hexagon,ia64,m32r,m68k,m68knommu,metag,microblaze,mn10300,openrisc,parisc,powerpc,ppc,s390,score,sh,sh64,sparc,sparc64,tile,unicore32,um,v850,xtensa}
  if [ "$CARCH" = "mips64el" ]; then
    rm -rf ${pkgdir}/usr/lib/modules/${_kernver}/build/arch/x86
  else
    rm -rf ${pkgdir}/usr/lib/modules/${_kernver}/build/arch/mips
  fi
}

_package-docs() {
  pkgdesc="Kernel hackers manual - HTML documentation that comes with the ${pkgbase^} kernel"
  provides=("kernel26${_kernelname}-docs=${pkgver}" "linux${_kernelname}-docs=${pkgver}")
  conflicts=("kernel26${_kernelname}-docs" "kernel26-libre${_kernelname}-docs" "linux${_kernelname}-docs")
  replaces=("kernel26${_kernelname}-docs" "kernel26-libre${_kernelname}-docs" "linux${_kernelname}-docs")

  cd "${srcdir}/linux-${_basekernel}"

  mkdir -p "${pkgdir}/usr/lib/modules/${_kernver}/build"
  cp -al Documentation "${pkgdir}/usr/lib/modules/${_kernver}/build"
  find "${pkgdir}" -type f -exec chmod 444 {} \;
  find "${pkgdir}" -type d -exec chmod 755 {} \;

  # remove a file already in linux package
  rm -f "${pkgdir}/usr/lib/modules/${_kernver}/build/Documentation/DocBook/Makefile"
}

pkgname=("${pkgbase}" "${pkgbase}-headers" "${pkgbase}-docs")
for _p in ${pkgname[@]}; do
  eval "package_${_p}() {
    _package${_p#${pkgbase}}
  }"
done

# vim:set ts=8 sts=2 sw=2 et:
md5sums=('7ec84223c7adcf01a5287eb7af96b7e7'
         'd09e8017d22370b873628942f3ece3b0'
         'b7718a81faf845ab53234336122ad631'
         'f7ee88ffc93b665345b6ee89ff192e1f'
         'e49ac236dfeef709f91a3d993ea7b62c'
         '2967cecc3af9f954ccc822fd63dca6ff'
         '8267264d9a8966e57fdacd1fa1fc65c4'
         '36e90334b12b2e9d53fd92129ee98f5b'
         '98beb36f9b8cf16e58de2483ea9985e3'
         'd50c1ac47394e9aec637002ef3392bd1'
         '1040ae6c10d4a68f89899f94a2318a17'
         'e92d4241b533fdd7165b5d47197f7162')
md5sums=('254f59707b6676b59ce5ca5c3c698319'
         '4b18ce60daff87504e6740670edf6b50'
         'aa6614d1dbf46ab7d317bcde1e58f80c'
         '3ecb40ee3bdd78e4d310c2afc1ccf209'
         'e49ac236dfeef709f91a3d993ea7b62c'
         '2967cecc3af9f954ccc822fd63dca6ff'
         '8267264d9a8966e57fdacd1fa1fc65c4'
         '44260d2cb1a8b51c119d2ce1f83e457a'
         '98beb36f9b8cf16e58de2483ea9985e3'
         'd50c1ac47394e9aec637002ef3392bd1'
         '71bbddd5f9799aa045660d92baa05845')
