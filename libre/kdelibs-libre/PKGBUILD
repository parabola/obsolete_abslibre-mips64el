# $Id: PKGBUILD 205088 2014-02-04 13:47:15Z svenstaro $
# Maintainer: Andrea Scarpino <andrea@archlinux.org
# Contributor: Pierre Schmitz <pierre@archlinux.de>
# Maintainer (Parabola): André Silva <emulatorman@parabola.nu>

_pkgname=kdelibs
pkgname=kdelibs-libre
pkgver=4.12.2
pkgrel=1
pkgdesc="KDE Core Libraries, without nonfree plugins recommendation support"
arch=('i686' 'x86_64' 'mips64el')
url='https://projects.kde.org/projects/kde/kdelibs'
license=('GPL' 'LGPL' 'FDL')
depends=('strigi' 'attica' 'libxss' 'soprano' 'krb5' 'grantlee'
        'shared-desktop-ontologies' 'qca' 'libdbusmenu-qt' 'polkit-qt'
        'shared-mime-info' 'enchant' 'giflib' 'jasper' 'openexr'
        'docbook-xsl' 'upower' 'udisks2' 'libxcursor' 'phonon-qt4'
        'media-player-info' 'libxtst' 'libutempter' 'qtwebkit')
makedepends=('cmake' 'automoc4' 'avahi' 'libgl' 'hspell' 'mesa')
provides=("kdelibs=$pkgver")
replaces=('kdelibs')
conflicts=('kdelibs')
install=${_pkgname}.install
source=("http://download.kde.org/stable/${pkgver}/src/${_pkgname}-${pkgver}.tar.xz"
        'kde-applications-menu.patch' 'qt4.patch' 
        'khtml-fsdg.diff')
sha1sums=('05e324af3752953ca637c2a51d989155f9f6bb8a'
          '86ee8c8660f19de8141ac99cd6943964d97a1ed7'
          'ed1f57ee661e5c7440efcaba7e51d2554709701c'
          'a1502a964081ad583a00cf90c56e74bf60121830')

prepare() {
       cd ${_pkgname}-${pkgver}
       # avoid file conflict with gnome-menus
       patch -p1 -i "${srcdir}"/kde-applications-menu.patch
       # qmake refers to Qt5
       patch -p1 -i "${srcdir}"/qt4.patch
       # Don't ask the user to download a plugin, it's probably nonfree.
       patch -p1 -i "${srcdir}"/khtml-fsdg.diff
}

build() {
       mkdir build
       cd build
       cmake ../${_pkgname}-${pkgver} \
               -DCMAKE_BUILD_TYPE=Release \
               -DKDE4_BUILD_TESTS=OFF \
               -DCMAKE_SKIP_RPATH=ON \
               -DKDE_DISTRIBUTION_TEXT='Parabola GNU/Linux-libre' \
               -DCMAKE_INSTALL_PREFIX=/usr \
               -DSYSCONF_INSTALL_DIR=/etc \
               -DHTML_INSTALL_DIR=/usr/share/doc/kde/html \
               -DKDE_DEFAULT_HOME='.kde4' \
               -DWITH_FAM=OFF \
               -DWITH_SOLID_UDISKS2=ON
       make
}

package() {
       cd "${srcdir}"/build
       make DESTDIR="${pkgdir}" install

       # cert bundle seems to be hardcoded
       # link it to the one from ca-certificates
       rm -f "${pkgdir}"/usr/share/apps/kssl/ca-bundle.crt
       ln -sf /etc/ssl/certs/ca-certificates.crt "${pkgdir}"/usr/share/apps/kssl/ca-bundle.crt
}
