# $Id: PKGBUILD 103873 2014-01-13 00:22:03Z kkeen $
# Maintainer: Kyle Keen <keenerd@gmail.com>
# Contributor: Christoph Zeiler <archNOSPAM_at_moonblade.dot.org>

_pkgname=spectrwm
pkgname=spectrwm-libre
pkgver=2.4.0
pkgrel=2
pkgdesc="A minimalistic automatic tiling window manager that tries to stay out of the way, without nonfree profont support"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.spectrwm.org"
_watch="https://opensource.conformal.com/snapshots/spectrwm/"
license=('custom:ISC')
depends=('dmenu' 'xcb-util' 'xcb-util-wm' 'xcb-util-keysyms' 'libxrandr' 'libxft' 'libxcursor')
replaces=('spectrwm' 'scrotwm')
conflicts=('spectrwm')
provides=("spectrwm=$pkgver")
makedepends=('libxt')
optdepends=('scrot: screenshots' 'xlockmore: great screenlocker' 'terminus-font: great font')
backup=(etc/spectrwm.conf)
source=(http://opensource.conformal.com/snapshots/$_pkgname/$_pkgname-$pkgver.tgz \
	LICENSE \
        baraction.sh \
        swmhack_path.patch \
        spectrwm-no-preload)
md5sums=('23e32c1e292f2fc94ed88195ffe60023'
         'a67cfe51079481e5b0eab1ad371379e3'
         '950d663692e1da56e0ac864c6c3ed80e'
         '8914dc2bef96e3587fc9317a25c1cbe8'
         '974d109ce0af39cc73936d5efd682480')

build() {
  cd "$srcdir/$_pkgname-$pkgver"
  
  sed -i 's|\"/usr/local/lib/libswmhack.so\"|\"libswmhack.so\"|' spectrwm.c
  sed -i 's/verbose_layout = 0;/verbose_layout = 1;/' spectrwm.c
  sed -i 's/# modkey = Mod1/modkey = Mod4/' spectrwm.conf
  # crashes if defaults can't be found, use less massive defaults
  sed -i 's/-\*-terminus-medium-\*-\*-\*-\*/-*-profont-*-*-*-*-12/' spectrwm.conf
  #sed -i 's/# program[lock].*/program[lock] = slock/' spectrwm.conf

  # see spectrwm FS#403
  sed -i 's/setconfspawn("lock".*/setconfspawn("lock", "xlock", SWM_SPAWN_OPTIONAL);/' spectrwm.c

  # FS#37998, remove on 2.4.1
  patch -p1 -i "$srcdir/swmhack_path.patch"

  cd linux
  make PREFIX="/usr"
}

package() {
  cd "$srcdir/$_pkgname-$pkgver/linux"
  make PREFIX="/usr" DESTDIR="$pkgdir" install
  install -Dm644 spectrwm.desktop "$pkgdir/usr/share/xsessions/spectrwm.desktop"
  cd ..
  install -Dm644 spectrwm.conf "$pkgdir/etc/spectrwm.conf"
  install -Dm755 screenshot.sh "$pkgdir/usr/share/spectrwm/screenshot.sh"
  mkdir -p "$pkgdir/etc/spectrwm"
  cp spectrwm_*.conf "$pkgdir/etc/spectrwm/"
  cd "$srcdir"
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$_pkgname/LICENSE"
  install -Dm755 baraction.sh "$pkgdir/usr/share/spectrwm/baraction.sh"
  install -Dm755 spectrwm-no-preload "$pkgdir/usr/bin/spectrwm-no-preload"

  #ln -s /usr/lib/libswmhack.so.0.0 "$pkgdir/usr/lib/libswmhack.so.0"
  #ln -s /usr/lib/libswmhack.so.0.0 "$pkgdir/usr/lib/libswmhack.so"

  # fix this for real in the makefile
  rm "$pkgdir/usr/bin/scrotwm"
  ln -s "/usr/bin/spectrwm" "$pkgdir/usr/bin/scrotwm"
  mkdir -p "$pkgdir"/usr/share/man/{es,it,pt,ru}/man1/
  mv "$pkgdir/usr/share/man/man1/spectrwm_es.1" "$pkgdir/usr/share/man/es/man1/"
  mv "$pkgdir/usr/share/man/man1/spectrwm_it.1" "$pkgdir/usr/share/man/it/man1/"
  mv "$pkgdir/usr/share/man/man1/spectrwm_pt.1" "$pkgdir/usr/share/man/pt/man1/"
  mv "$pkgdir/usr/share/man/man1/spectrwm_ru.1" "$pkgdir/usr/share/man/ru/man1/"
}
