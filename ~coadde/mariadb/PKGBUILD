# $Id$
# Maintainer:
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Douglas Soares de Andrade <douglas@archlinux.org>
# Maintainer (Parabola): Márcio Silva <coadde@lavabit.com>

falsename=mysql
_falsename=MySQL
pkgbase=mariadb
_pkgbase=MariaDB
pkgname=("lib${pkgbase}client" "${pkgbase}-clients" "${pkgbase}")
pkgver=5.5.23
pkgrel=1.5
arch=('i686' 'x86_64' 'mips64el')
license=('GPL')
url="http://www.${pkgbase}.org/"
makedepends=('cmake' 'openssl' 'zlib')
options=('!libtool')
source=("http://mirror.aarnet.edu.au/pub/${_pkgbase}/${pkgbase}-${pkgver}/kvm-tarbake-jaunty-x86/${pkgbase}-${pkgver}.tar.gz"
        "${falsename}d"
        'my.cnf')
md5sums=('7074fa091b3c1489f45a5ddf12cd5e6f'
         '2234207625baa29b2ff7d7b4f088abce'
         '1c949c0dbea5206af0db14942d9927b6')

build() {
  cd "${srcdir}"
  mkdir build
  cd build

  # CFLAGS/CXXFLAGS as suggested upstream
  CFLAGS="-fPIC ${CFLAGS} -fno-strict-aliasing -DBIG_JOINS=1 -fomit-frame-pointer" \
  CXXFLAGS="-fPIC ${CXXFLAGS} -fno-strict-aliasing -DBIG_JOINS=1 -felide-constructors -fno-rtti" \

  cmake ../${pkgbase}-${pkgver} \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DSYSCONFDIR=/etc/${falsename} \
    -DMYSQL_DATADIR=/var/lib/${falsename} \
    -DMYSQL_UNIX_ADDR=/var/run/${falsename}/${falsename}.sock \
    -DDEFAULT_CHARSET=utf8 \
    -DDEFAULT_COLLATION=utf8_unicode_ci \
    -DENABLED_LOCAL_INFILE=ON \
    -DINSTALL_INFODIR=share/${falsename}/docs \
    -DINSTALL_MANDIR=share/man \
    -DINSTALL_PLUGINDIR=/usr/lib/${falsename}/plugin \
    -DINSTALL_SCRIPTDIR=bin \
    -DINSTALL_INCLUDEDIR=include/${falsename} \
    -DINSTALL_DOCREADMEDIR=share/${falsename} \
    -DINSTALL_SUPPORTFILESDIR=share/${falsename} \
    -DINSTALL_MYSQLSHAREDIR=share/${falsename} \
    -DINSTALL_DOCDIR=share/${falsename}/docs \
    -DINSTALL_SHAREDIR=share/${falsename} \
    -DWITH_READLINE=ON \
    -DWITH_ZLIB=system \
    -DWITH_SSL=system \
    -DWITH_LIBWRAP=OFF \
    -DWITH_MYSQLD_LDFLAGS="${LDFLAGS}" \
    -DWITH_EXTRA_CHARSETS=complex \
    -DWITH_EMBEDDED_SERVER=ON \
    -DWITH_INNOBASE_STORAGE_ENGINE=1 \
    -DWITH_PARTITION_STORAGE_ENGINE=1 \
    -DWITH_PBXT_STORAGE_ENGINE=1 \
    -DWITHOUT_EXAMPLE_STORAGE_ENGINE=1 \
    -DWITHOUT_ARCHIVE_STORAGE_ENGINE=1 \
    -DWITHOUT_BLACKHOLE_STORAGE_ENGINE=1 \
    -DWITHOUT_FEDERATED_STORAGE_ENGINE=1

  make
}

package_libmariadbclient(){
  pkgdesc="${_pkgbase} client libraries (branch of ${_falsename})"
  depends=('openssl')
  conflicts=("lib${falsename}client")
  provides=("lib${falsename}client=${pkgver}")
  
  cd "${srcdir}"/build
  for dir in include lib${falsename} lib${falsename}d libservices; do
    make -C ${dir} DESTDIR="${pkgdir}" install
  done

  install -d "${pkgdir}"/usr/bin
  install -m755 scripts/${falsename}_config "${pkgdir}"/usr/bin/
  install -d "${pkgdir}"/usr/share/man/man1
  for man in ${falsename}_config ${falsename}_client_test_embedded ${falsename}test_embedded; do
    install -m644 "${srcdir}"/${pkgbase}-${pkgver}/man/$man.1 "${pkgdir}"/usr/share/man/man1/$man.1
  done
}

package_mariadb-clients(){
  pkgdesc="${_pkgbase} client tools (branch of ${_falsename})"
  depends=("lib${pkgbase}client")
  conflicts=("${falsename}-clients")
  provides=("${falsename}-clients=${pkgver}")

  cd "${srcdir}"/build
  make -C client DESTDIR="${pkgdir}" install

  # install man pages
  install -d "${pkgdir}"/usr/share/man/man1
  for man in ${falsename} ${falsename}admin ${falsename}check ${falsename}dump ${falsename}import ${falsename}show ${falsename}slap; do
    install -m644 "${srcdir}"/${pkgbase}-${pkgver}/man/$man.1 "${pkgdir}"/usr/share/man/man1/$man.1
  done

  # provided by mariadb
  rm "${pkgdir}"/usr/bin/{${falsename}_{plugin,upgrade},${falsename}binlog,${falsename}test}
}

package_mariadb(){
  pkgdesc="A fast SQL database server branch of ${_falsename} - ${_pkgbase}"
  backup=("etc/${pkgbase}/my.cnf")
  install=${falsename}.install
  depends=("${pkgbase}-clients")
  optdepends=('perl-dbi' "perl-dbd-${falsename}")
  conflicts=("${falsename}")
  provides=("${falsename}=${pkgver}")
  options=('emptydirs')

  cd "${srcdir}"/build
  make DESTDIR="${pkgdir}" install

  install -Dm644 "${srcdir}"/my.cnf "${pkgdir}"/etc/${falsename}/my.cnf
  install -Dm755 "${srcdir}"/${falsename}d "${pkgdir}"/etc/rc.d/${falsename}d

  # provided by libmariadbclient
  rm "${pkgdir}"/usr/bin/{${falsename}_config,${falsename}_client_test_embedded,${falsename}test_embedded}
  rm "${pkgdir}"/usr/lib/lib${falsename}*
  rm -r "${pkgdir}"/usr/include/
  rm "${pkgdir}"/usr/share/man/man1/{${falsename}_config,${falsename}_client_test_embedded,${falsename}test_embedded}.1
  
  # provided by mariadb-clients
  rm "${pkgdir}"/usr/bin/{${falsename},${falsename}admin,${falsename}check,${falsename}dump,${falsename}import,${falsename}show,${falsename}slap}
  rm "${pkgdir}"/usr/share/man/man1/{${falsename},${falsename}admin,${falsename}check,${falsename}dump,${falsename}import,${falsename}show,${falsename}slap}.1

  # not needed
  rm -r "${pkgdir}"/usr/{data,${falsename}-test,sql-bench}
  rm "${pkgdir}"/usr/share/man/man1/${falsename}-test-run.pl.1

  install -dm700 "${pkgdir}"/var/lib/${falsename}
}
