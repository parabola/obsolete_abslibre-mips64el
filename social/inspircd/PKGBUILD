pkgname=inspircd
pkgver=2.0.5
pkgrel=2
pkgdesc='A lightweight IRC daemon'
arch=('x86_64' 'i686')
url='http://www.inspircd.org/'
license=('GPL2')
depends=('perl' 'gnutls' 'openssl' 'libgcrypt')
makedepends=('pkg-config' 'mysql' 'postgresql' 'sqlite3' 'libldap' 'geoip' 'tre')
optdepends=('mysql: m_mysql'
            'pcre: m_regex_pcre'
            'postgresql: m_pgsql'
            'sqlite3: m_sqlite3'
            'libldap: m_ldapoper and m_ldapauth'
            'geoip: m_geoip'
            'tre: m_regex_tre')
install=inspircd.install
backup=('etc/inspircd/inspircd.conf')

source=(http://www.inspircd.org/downloads/InspIRCd-$pkgver.tar.bz2 inspircd.rcd gnutls.patch)
md5sums=('60dec04bdc8f8c473f3c7bd774a1f153'
         'f4f32d944401b1602ab6716476892afd'
         '6312154e759e5d71b85c7dca71b7a4fd')

build() {
  cd "${srcdir}/inspircd"
  patch -Np1 -i "${srcdir}/gnutls.patch"

  ./configure \
    --enable-extras=m_geoip.cpp \
    --enable-extras=m_ldapauth.cpp \
    --enable-extras=m_ldapoper.cpp \
    --enable-extras=m_mysql.cpp \
    --enable-extras=m_pgsql.cpp \
    --enable-extras=m_regex_pcre.cpp \
    --enable-extras=m_regex_posix.cpp \
    --enable-extras=m_regex_tre.cpp \
    --enable-extras=m_sqlite3.cpp

  ./configure \
    --prefix=/usr/lib/inspircd \
    --binary-dir=/usr/sbin \
    --module-dir=/usr/lib/inspircd/modules \
    --config-dir=/etc/inspircd \
    --enable-gnutls \
    --enable-openssl \
    --enable-epoll

  make
}

package() {
  install -Dm755 "${srcdir}"/inspircd.rcd "${pkgdir}"/etc/rc.d/inspircd
  install -o141 -g141 -dm750 "${pkgdir}/var/log/inspircd"
  install -o141 -g141 -dm750 "${pkgdir}/var/run/inspircd"

  cd "${srcdir}/inspircd"
  make DESTDIR="$pkgdir" INSTUID=141 install

  rm -rf "${pkgdir}"/usr/lib/inspircd/logs
  rm -rf "${pkgdir}"/usr/lib/inspircd/data
}
