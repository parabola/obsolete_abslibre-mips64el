#!/usr/bin/env python

from distutils.core import setup, Extension
import os
import sys

old_filename = os.path.join(os.path.curdir, "RNA.py")
new_filename = os.path.join(os.path.curdir, "__init__.py")
if os.path.exists(old_filename):
    os.rename(old_filename, new_filename)

extra_link_args = []
if sys.platform != 'darwin':
    extra_link_args.append('-s')

extension = Extension("_RNA",
                      ["RNA_wrap.c"],
                      libraries=['RNA'],
                      library_dirs=['lib'],
                      extra_link_args=extra_link_args
                      )

setup(name="RNA",
      version="1.8.5",
      description="Vienna RNA",
      author="Ivo Hofacker, Institute for Theoretical Chemistry, University of Vienna",
      url="http://www.tbi.univie.ac.at/RNA/",
      package_dir = {'vienna': os.path.curdir},
      packages = ['vienna'],
      ext_modules=[extension],
      )
