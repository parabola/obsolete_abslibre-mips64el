# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>
# Contributor: pallegro from the AUR

pkgname=openni-module-sensorkinect-git
pkgver=20111211
pkgrel=2
pkgdesc="OpenNI SensorKinect harware modules for the Xbox Kinect device"
arch=('i686' 'x86_64')
url="https://github.com/avin2/SensorKinect"
license=('GPL')
depends=('openni-unstable' 'kinect-udev')
makedepends=('git')
provides=('openni-module-sensorkinect')
conflicts=('openni-module-sensorkinect' 'openni-module-primesensor')
install="sensorkinect.install"
source=()
md5sums=()

_gitroot="https://github.com/avin2/SensorKinect.git"
_gitname="sensorkinect"
_gitbranch="master"

_os='Linux'
case "${CARCH}" in
'x86_64') _arch=x86;;
'i686')   _arch=x86;;
'arm')    _arch=Arm;; # untested
*)        _arch=UNSUPPORTED_ARCH;;
esac
_platform="${_os}-${_arch}"

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [ -d ${_gitname} ] ; then
    cd ${_gitname}
    git checkout master
    git pull origin
    msg "The local files are updated."
  else
    git clone ${_gitroot} ${_gitname}
    cd ${_gitname}
  fi
  git checkout ${_gitbranch}
  cd ..

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd $srcdir/$_gitname-build

  # BUILD
  cd Platform/${_platform}/Build
  make
  make redist
}

package() {
  cd $srcdir/$_gitname-build
  cd Platform/${_platform}/Redist

  # This is all cloned from ./install.sh, which can't be used for a number
  # of reasons.
  install -d $pkgdir/usr/{lib,bin}
  install -m755 Lib/* $pkgdir/usr/lib
  install -m755 Bin/* $pkgdir/usr/bin

  install -d $pkgdir/etc/primesense
  install -m644 Config/GlobalDefaultsKinect.ini $pkgdir/etc/primesense

  chmod +s $pkgdir/usr/bin/XnSensorServer

  install -m777 -d $pkgdir/var/log/primesense/XnSensorServer
}
