# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>
# Contributor: Sven Schneider <archlinux.sandmann@googlemail.com>

# I am not giving libfreenect nearly the attention it deserves.
# There are a lot of options for it that I'm ignoring.
# Right now I'm focussing more on sensorkinect.
# Patches welcome!

pkgname=("libfreenect-git" 'kinect-udev-git')
pkgver=20111211
pkgrel=1
pkgdesc="OpenKinect drivers and libraries for the Xbox Kinect device"
arch=('i686' 'x86_64')
url="http://openkinect.org"
license=('GPL')
makedepends=('cmake' 'git' 'python2')
source=()
md5sums=()

_gitroot="git://github.com/OpenKinect/libfreenect.git"
_gitname=libfreenect

build() {
  cd "$srcdir"
  msg "Connecting to GIT server...."

  if [[ -d "$_gitname" ]]; then
    cd "$_gitname" && git pull origin
    msg "The local files are updated."
  else
    git clone "$_gitroot" "$_gitname"
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting build..."

  rm -rf "$srcdir/$_gitname-build"
  git clone "$srcdir/$_gitname" "$srcdir/$_gitname-build"
  cd "$srcdir/$_gitname-build"

  #
  # BUILD HERE
  #

  # Install "libfreenect.hpp" to "/usr/include/libfreenect"
  sed 's/DESTINATION include/DESTINATION include\/${projectNameLower}/g' -i "wrappers/cpp/CMakeLists.txt"
  
  export DYLD_FRAMEWORK_PATH=/usr/lib/python2.7/site-packages
  
  mkdir build
  cd build
  cmake \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DBUILD_REDIST_PACKAGE=ON \
      -DLIB_SUFFIX="" \
      -DBUILD_AUDIO=OFF \
      -DBUILD_PYTHON=OFF \
      -DPYTHON_EXECUTABLE="`which python2`" \
      ..
  make
}

package_libfreenect-git() {
  depends=('libusb' 'freeglut' 'libxmu' 'kinect-udev')
  provides=(libfreenect)
  conflicts=(libfreenect)

  # Make install
  cd "${srcdir}/${_gitname}-build/build"
  make DESTDIR="${pkgdir}" install

  # Patch include files
  sed 's/<libfreenect.h>/<libfreenect\/libfreenect.h>/g' -i "${pkgdir}/usr/include/libfreenect/libfreenect.hpp"
}

package_kinect-udev-git() {
  pkgdesc="Udev rules for the Xbox Kinect device"
  depends=('udev')
  provides=(kinect-udev)
  conflicts=(kinect-udev)

  # Install udev rules
  cd "${srcdir}/${_gitname}-build/"
  install -d "${pkgdir}/etc/udev/rules.d"
  install -m644 platform/linux/udev/51-kinect.rules "${pkgdir}/etc/udev/rules.d"
}