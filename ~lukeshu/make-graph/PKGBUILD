# Maintainer (Parabola): Luke Shumaker <lukeshu@sbcglobal.net>
# Maintainer (Arch): Allan McRae <allan@archlinux.org>
# Contributor: judd <jvinet@zeroflux.org>

_pkgname=make
pkgname=$_pkgname-graph
pkgver=3.82
pkgrel=4.2
provides=("$_pkgname=$pkgver")
conflicts=("$_pkgname")
pkgdesc="GNU make utility to maintain groups of programs, with patch to dump a Graphviz dependency graph."
arch=('i686' 'x86_64')
url="http://www.gnu.org/software/make"
license=('GPL3')
#groups=('base-devel')
depends=('glibc' 'sh')
install=$_pkgname.install
source=(ftp://ftp.gnu.org/gnu/${_pkgname}/${_pkgname}-${pkgver}.tar.bz2
        bug30612.patch
        bug30723.patch
        make-3.82-sort-blank.patch
        make-3.82-makeflags.patch)
md5sums=('1a11100f3c63fcf5753818e59d63088f'
         'c8f496b22191f9fb9420ab14c1a19a47'
         '662e6450e19a5acdaa5c9fcb8ad78dea'
         '7d01a99f389d8f08dec93ed479071ee4'
         'bc12ad4d0c6e6c0e72d9fb61054f446b')

source+=('make-graph.patch')
md5sums+=('c1f1d652004a36c69b1f7f42a6849356')
depends+=('graphviz')

build() {
  cd ${srcdir}/${_pkgname}-${pkgver}
  
  # http://savannah.gnu.org/bugs/?30612
  patch -Np1 -i $srcdir/bug30612.patch
  
  # http://savannah.gnu.org/bugs/?30723
  patch -Np1 -i $srcdir/bug30723.patch
  
  # https://bugs.archlinux.org/task/22733 (fix from Fedora)
  patch -Np1 -i $srcdir/make-3.82-sort-blank.patch
  
  # https://savannah.gnu.org/support/index.php?107487
  # https://savannah.gnu.org/bugs/?33873
  patch -Np0 -i $srcdir/make-3.82-makeflags.patch

  # https://savannah.gnu.org/patch/?7447
  patch -Np1 -i $srcdir/make-graph.patch
  
  ./configure --prefix=/usr
  make
}

check() {
  cd ${srcdir}/${_pkgname}-${pkgver}
  make check
}

package() {
  cd ${srcdir}/${_pkgname}-${pkgver}
  make DESTDIR=${pkgdir} install
}
