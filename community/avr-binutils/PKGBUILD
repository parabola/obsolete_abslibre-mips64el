# $Id: PKGBUILD 99691 2013-10-30 23:03:14Z allan $
# Maintainer: schuay <jakob.gruber@gmail.com>
# Contributor: Brad Fanella <bradfanella@archlinux.us>
# Contributor: Corrado Primier <bardo@aur.archlinux.org>
# Contributor: danst0 <danst0@west.de>

# Build order: avr-binutils -> avr-gcc -> avr-libc

pkgname=avr-binutils
pkgver=2.23.2
pkgrel=1
pkgdesc="A set of programs to assemble and manipulate binary and object files for the AVR architecture"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.gnu.org/software/binutils/"
license=('GPL')
depends=('glibc>=2.17' 'zlib' 'binutils')
provides=("binutils-avr=$pkgver")
replaces=('binutils-avr')
options=('!distcc' '!ccache')
install=avr-binutils.install
source=(ftp://ftp.gnu.org/gnu/binutils/binutils-${pkgver}.tar.bz2{,.sig}
        binutils-2.23.2-texinfo-5.0.patch
        avr-size.patch)

_builddir=binutils-build

build() {
    cd ${srcdir}/binutils-${pkgver}

    # See https://bugs.archlinux.org/task/32504
    patch -Np0 < ${srcdir}/avr-size.patch

    # http://sourceware.org/git/?p=binutils.git;a=patch;h=e02bf935
    # http://sourceware.org/git/?p=binutils.git;a=patch;h=935f8542
    patch -p1 -i ${srcdir}/binutils-2.23.2-texinfo-5.0.patch

    # https://bugs.archlinux.org/task/34629
    sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure

    config_guess=$(./config.guess)

    cd ${srcdir}
    rm -rf ${_builddir}
    mkdir ${_builddir} && cd ${_builddir}

    ${srcdir}/binutils-${pkgver}/configure \
        --prefix=/usr \
        --with-lib-path=/usr/lib:/usr/local/lib \
        --with-bugurl=https://bugs.archlinux.org/ \
        --enable-ld=default \
        --enable-gold \
        --enable-plugins \
        --enable-threads \
        --with-pic \
        --enable-shared \
        --disable-werror \
        --disable-multilib \
        --build=${config_guess} \
        --target=avr \
        $CONFIGFLAG

    # This checks the host environment and makes sure all the necessary tools are available to compile Binutils.
    make configure-host

    make tooldir=/usr
}

package() {
    cd ${srcdir}/${_builddir}

    make prefix=${pkgdir}/usr tooldir=${pkgdir}/usr install

    rm ${pkgdir}/usr/lib/libiberty.a

    for bin in ar as nm objcopy objdump ranlib strip ; do
        rm -f ${pkgdir}/usr/bin/${bin}
    done

    for info in as bfd binutils configure gprof ld standards; do
        mv ${pkgdir}/usr/share/info/${info}.info ${pkgdir}/usr/share/info/avr-${info}.info
    done

    rm -rf ${pkgdir}/usr/share/locale
}

md5sums=('4f8fa651e35ef262edc01d60fb45702e'
         'SKIP'
         '34e439ce23213a91e2af872dfbb5094c'
         '573769ec1107d1bfed78680edd7ae63f')
