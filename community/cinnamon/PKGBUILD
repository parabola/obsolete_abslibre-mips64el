# $Id: PKGBUILD 103395 2014-01-05 16:56:22Z bgyorgy $
# Maintainer: Alexandre Filgueira <alexfilgueira@cinnarch.com>
# Contributor: M0Rf30
# Contributor: unifiedlinux
# Contributor: CReimer

pkgname=cinnamon
pkgver=2.0.14
pkgrel=2
pkgdesc="Linux desktop which provides advanced innovative features and a traditional user experience"
arch=('i686' 'x86_64' 'mips64el')
url="http://cinnamon.linuxmint.com/"
license=('GPL2')
depends=('accountsservice' 'caribou' 'cinnamon-settings-daemon' 'cinnamon-session'
         'cinnamon-translations' 'cjs' 'clutter-gtk' 'gconf' 'gnome-icon-theme'
         'gnome-menus' 'gnome-themes-standard' 'gstreamer' 'libgnome-keyring'
         'librsvg' 'networkmanager' 'muffin' 'python2-dbus' 'python2-pillow'
         'python2-pam' 'python2-pexpect' 'python2-pyinotify' 'python2-lxml' 'webkitgtk'
         'cinnamon-control-center' 'cinnamon-screensaver' 'libgnomekbd'
         'network-manager-applet' 'nemo' 'polkit-gnome')
makedepends=('gnome-common' 'intltool')
options=('!emptydirs')
install=${pkgname}.install
source=("$pkgname-$pkgver.tar.gz::https://github.com/linuxmint/Cinnamon/archive/$pkgver.tar.gz"
        "cinnamon-gstreamer1.patch"
        "keyboard_applet.patch"
        "input_keybindings.patch"
        "gtk3-lock-dialog.patch"
        "org.archlinux.pkexec.cinnamon-settings-users.policy")
sha256sums=('447f65ceb22a40717656d3d708d16ad450802298d783b23004b6e8d3c8d7da85'
            '2e10ba71fd9ba40afd7e9492b2fc0d5bcc27874bcde543cfd8a47ad20c52354a'
            '6acb07393105ddced8a4c3c869a596350d1a7d81a808ca5307d2ad770653a9d3'
            'e28c40eb844105154fa6106f5b4de3151a22805b3a7b2f84be9ea6c15cec3de6'
            'e10dd01201b9274b45a6c94319d4eb1e9ff800e63f10a3d5e42fc9f3a87e337c'
            '371beac9e55d36f7629d2fc5cb40d6a3e6c0f4aac014f6fefdcd6743b5194b23')

prepare() {
  cd ${srcdir}/Cinnamon*

  # Python2 fix
  sed -i 's:/usr/bin/python :/usr/bin/python2 :' files/usr/bin/cinnamon-menu-editor
  find -type f | xargs sed -i 's@^#!.*python$@#!/usr/bin/python2@'

  # Fix keyboard applet
  patch -Np1 -i ../keyboard_applet.patch

  # Add input keybindings
  patch -Np1 -i ../input_keybindings.patch

  # Port to GStreamer 1.0. Also backports some recorder bugfixes
  patch -Np1 -i ../cinnamon-gstreamer1.patch

  # Port Lock screen dialog to GTK+ 3
  patch -Np1 -i ../gtk3-lock-dialog.patch

  # Fix required components
  sed -i 's/cinnamon-fallback-mount-helper;/polkit-gnome-authentication-agent-1;/' files/usr/share/cinnamon-session/sessions/cinnamon*.session

  # fix for the python2 PAM module  
  sed -i 's:import PAM:import pam:' files/usr/lib/cinnamon-settings/modules/cs_user.py

  # Use pkexec instead of gksu
  sed -i 's/gksu/pkexec/' files/usr/bin/cinnamon-settings-users

  # Check for the cc-panel path, not for the unneeded binary
  sed -i 's|/usr/bin/cinnamon-control-center|/usr/lib/cinnamon-control-center-1/panels|' files/usr/bin/cinnamon-settings

  # Cinnamon has no upstream backgrounds, use GNOME backgrounds instead
  sed -i 's|/usr/share/cinnamon-background-properties|/usr/share/gnome-background-properties|' \
    files/usr/lib/cinnamon-settings/modules/cs_backgrounds.py

  # Prefix 'System Settings' with 'Cinnamon' to avoid confusion with gnome-control-center
  sed -i 's/^Name\(.*\)=\(.*\)/Name\1=Cinnamon \2/' files/usr/share/applications/cinnamon-settings{,-users}.desktop
}

build() {
  cd ${srcdir}/Cinnamon*

  ./autogen.sh --prefix=/usr \
               --sysconfdir=/etc \
               --libexecdir=/usr/lib/cinnamon \
               --localstatedir=/var \
               --disable-static \
               --disable-schemas-compile \
               --enable-compile-warnings=yes \
               --with-session-tracking=systemd
  make
}

package() {
  cd ${srcdir}/Cinnamon*
  make DESTDIR="${pkgdir}" install

  # Install policy file
  install -Dm644 "${srcdir}/org.archlinux.pkexec.cinnamon-settings-users.policy" \
                 "${pkgdir}/usr/share/polkit-1/actions/org.archlinux.pkexec.cinnamon-settings-users.policy"
}
