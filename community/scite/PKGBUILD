# $Id: PKGBUILD 105192 2014-02-02 16:50:19Z arodseth $
# Maintainer: Alexander Rødseth <rodseth@gmail.com>
# Contributor: Thomas S Hatch <thatch45@gmail.com>
# Contributor: Corrado 'bardo' Primier <corrado.primier@mail.polimi.it>
# Contributor: Jochem Kossen <j.kossen@home.nl>
# Contributor: Daniel J Griffiths <ghost1227@archlinux.us>

pkgname=scite
pkgver=3.3.9
pkgrel=1
pkgdesc='Editor with facilities for building and running programs'
arch=('i686' 'x86_64' 'mips64el')
url='http://www.scintilla.org/SciTE.html'
license=('custom:scite')
depends=('desktop-file-utils' 'gtk2')
makedepends=('setconf')
backup=('usr/share/scite/SciTEGlobal.properties')
install="$pkgname.install"
source=("http://downloads.sourceforge.net/sourceforge/scintilla/${pkgname}${pkgver//./}.tgz")
sha256sums=('cd7bdfdf8cd870893375519405510417145ec0a9790ed7209f21d552d36c0775')

prepare() {
  if [ $?CXXBASEFLAGS == 1 ]; then 
    sed '0,/CXXTFLAGS=/s//nop=/' -i scite/gtk/makefile
    setconf scite/gtk/makefile CXXTFLAGS "-DNDEBUG ${CXXFLAGS} $(CXXBASEFLAGS)"
  fi
  sed '0,/CXXFLAGS=/s//nop=/' -i scintilla/gtk/makefile
  setconf scintilla/gtk/makefile CXXFLAGS \
    "-DNDEBUG ${CXXFLAGS} \$(CXXBASEFLAGS) \$(THREADFLAGS)"
  setconf scintilla/gtk/makefile CXXBASEFLAGS \
    "-Wno-missing-braces -Wno-char-subscripts ${CXXFLAGS} -DGTK -DSCI_LEXER \$(INCLUDEDIRS)"
}

build() {
  make -C "scintilla/gtk"
  make -C "$pkgname/gtk"
}

package() {
  cd "$pkgname/gtk"

  make DESTDIR="$pkgdir" install
  install -Dm644 "$srcdir/$pkgname/License.txt" \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE-scite"
  install -Dm644 "$srcdir/scintilla/License.txt" \
    "$pkgdir/usr/share/licenses/$pkgname/LICENSE-scintilla"
  ln -sf SciTE "$pkgdir/usr/bin/scite"
}

# vim:set ts=2 sw=2 et:
