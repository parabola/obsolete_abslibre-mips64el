# $Id: PKGBUILD 103609 2014-01-08 10:39:22Z arcanis $
# Maintainer: Evgeniy Alekseev <arcanis.arch at gmail dot com>
# Contributor: Matt Harrison <matt at mistbyte dot com>
# Contributor: Muhammad Qadri <Muhammad dot A dot Qadri at gmail dot com>

pkgname=julius
pkgver=4.3
pkgrel=1
pkgdesc="A high-performance, two-pass large vocabulary continuous speech recognition decoder software"
arch=("i686" "x86_64")
url="http://julius.sourceforge.jp/"
license=("custom")
depends=("libpulse")
optdepends=("voxforge-am-julius: acoustic models")
source=("http://iij.dl.sourceforge.jp/${pkgname}/60153/${pkgname}-${pkgver}.tar.gz")
md5sums=("48b36e9104b55c409a60947f7ba05bb3")

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  
  # replace /lib/cpp with /usr/bin/cpp since Arch Linux doesn't have /lib/cpp anymore
  sed -i "s|/lib/cpp|/usr/bin/cpp|g" "julius/configure"
  sed -i "s|/lib/cpp|/usr/bin/cpp|g" "jcontrol/configure"
  sed -i "s|/lib/cpp|/usr/bin/cpp|g" "libjulius/configure"
  sed -i "s|/lib/cpp|/usr/bin/cpp|g" "gramtools/configure"
  sed -i "s|/lib/cpp|/usr/bin/cpp|g" "libsent/configure"
  sed -i "s|/lib/cpp|/usr/bin/cpp|g" "configure"
  
  # Add -02 to CPPFLAGS to prevent issue with -D_FORTIFY_SOURCE=2
  CPPFLAGS="$CPPFLAGS -O2"
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}/"
  ./configure \
    --prefix="/usr/" \
    --mandir='${prefix}/share/man' \
    --enable-words-int \
    --enable-msd \
    --enable-gmm-vad \
    --enable-decoder-vad \
    --enable-power-reject \
    --enable-wpair
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}/"
  
  make prefix="${pkgdir}/usr" install
  install -D "LICENSE.txt" "${pkgdir}/usr/share/licenses/julius/LICENSE"
  # remove empty lib directory
  rm -r "${pkgdir}/usr/lib"
}

# vim:set ts=2 sw=2 et:
