# Maintainer: Martin Schmölzer <mschmoelzer@gmail.com>

_target=arm-none-eabi
pkgname=$_target-gcc
pkgver=4.8.2
pkgrel=8
_newlibver=2.1.0
pkgdesc='The GNU Compiler Collection - cross compiler for ARM EABI (bare-metal) target'
arch=(i686 x86_64)
url='http://gcc.gnu.org/'
license=(GPL LGPL FDL)
depends=(cloog libmpc zlib)
makedepends=($_target-binutils gmp mpfr)
#checkdepends=(dejagnu)
options=(!emptydirs !strip staticlibs)
source=(ftp://ftp.gnu.org/gnu/gcc/gcc-$pkgver/gcc-$pkgver.tar.bz2
        ftp://sourceware.org/pub/newlib/newlib-$_newlibver.tar.gz
        gcc-$pkgver-multilib.patch
        gcc-$pkgver-no-exceptions.patch
        libgloss-configure-fix.patch)
sha256sums=('09dc2276c73424bbbfda1dbddc62bbbf900c9f185acf7f3e1d773ce2d7e3cdc8'
            '3e4d5ab9f0508942b6231b8ade4f8e5048cf92c96ed574c2bd6bd3320a599a48'
            '4edd875c89391cbf5c2b57587a51da0dfda54ca0c711a85e9c301f03083e4189'
            '649eb3b443e975d40ca61128ba715d1e3a29716e1e45a52c661ac0dba3c2099d'
            'eac96c034d3d7d16080d9aef4749e4eda493563a3348e6a06f08e8a330b027ba')

prepare() {
  # Apply configure fix from newlib CVS
  cd "$srcdir/newlib-$_newlibver/libgloss/arm"
  patch -Np0 -i "$srcdir/libgloss-configure-fix.patch"
  autoreconf

  cd "$srcdir"
  patch -Np0 -i gcc-$pkgver-multilib.patch
  patch -Np0 -i gcc-$pkgver-no-exceptions.patch

  # Move newlib and libgloss to gcc source directory
  mv newlib-$_newlibver/newlib   gcc-$pkgver
  mv newlib-$_newlibver/libgloss gcc-$pkgver

  # hack! - some configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" gcc-$pkgver/{libiberty,gcc}/configure
}

build() {
  cd gcc-$pkgver
  mkdir build
  cd build

  export CFLAGS_FOR_TARGET='-O2 -g -pipe'
  export CXXFLAGS_FOR_TARGET='-O2 -g -pipe'

  ../configure --target=$_target \
               --prefix=/usr \
               --libexecdir=/usr/lib \
               --with-pkgversion='Arch Repository' \
               --with-bugurl='https://bugs.archlinux.org/' \
               --enable-multilib \
               --enable-interwork \
               --enable-languages=c,c++ \
               --with-newlib \
               --with-gnu-as \
               --with-gnu-ld \
               --disable-nls \
               --disable-shared \
               --disable-threads \
               --with-headers=newlib/libc/include \
               --disable-libssp \
               --disable-libstdcxx-pch \
               --disable-libmudflap \
               --disable-libgomp \
               --with-system-zlib \
               --disable-newlib-supplied-syscalls

  make
}

check() {
  cd gcc-$pkgver/build
# TODO: enable gcc tests for arm target
#  make check RUNTESTFLAGS="--target_board=arm-sim\{-mhard-float,-msoft-float\}\{-O2,\}"
#  ../contrib/test_summary
}

package() {
  cd gcc-$pkgver/build

  make DESTDIR="$pkgdir" install -j1

  # Strip host objects but do not touch target one
  find "$pkgdir" -executable -type f -exec /usr/bin/strip --strip-debug '{}' \;

  # Remove files that conflict with host gcc package
  rm "$pkgdir"/usr/lib/libiberty.a
  rm -r "$pkgdir"/usr/share/man/man7
  rm -r "$pkgdir"/usr/share/info
  rm -r "$pkgdir"/usr/share/gcc-$pkgver
}
