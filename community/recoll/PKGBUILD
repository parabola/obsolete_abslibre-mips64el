# $Id: PKGBUILD 101662 2013-11-29 20:55:41Z spupykin $
# Maintainer: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Daniel J Griffiths <ghost1227@archlinux.us>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Vladimir Chizhov <jagoterr@gmail.com>
# Contributor: Robert Emil Berge <filoktetes@linuxophic.org>

pkgname=recoll
pkgver=1.19.11_p1
pkgrel=1
pkgdesc="Full text search tool based on Xapian backend"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.lesbonscomptes.com/recoll/"
license=('GPL')
depends=('xapian-core>=1.0.15-1' 'qt4' 'openssl' 'hicolor-icon-theme' 'qtwebkit')
makedepends=('python2')
optdepends=('libxslt: for XML based formats (fb2,etc)'
	    'unzip: for the OpenOffice.org documents'
	    'poppler: for pdf'
	    'pstotext: for postscipt'
	    'antiword: for msword'
	    'catdoc: for ms excel and powerpoint'
	    'unrtf: for RTF'
	    'untex: for dvi support with dvips'
	    'djvulibre: for djvu'
	    'id3lib: for mp3 tags support with id3info'
	    'python2: for using some filters'
	    'mutagen: Audio metadata'
	    'python2-pychm: CHM files'
	    'perl-image-exiftool: EXIF data from raw files'
	    'aspell-en: English stemming support')
install=recoll.install
source=("http://www.lesbonscomptes.com/$pkgname/$pkgname-${pkgver/_/}.tar.gz")
md5sums=('5d77a74cddba4e21f7f3bb06ad20a7ac')

build() {
  cd "$srcdir/$pkgname-${pkgver/_/}"

  msg2 "Python2 fix"
  export PYTHON=/usr/bin/python2
  for file in filters/{rclchm,rclexecm.py,rclics,rclpython,rclzip,rclaudio,rclinfo,rclkar,rcllatinclass.py,rclwar,rclrar} \
              doc/user/usermanual.html; do
      sed -i 's_#!/usr/bin/env python_#!/usr/bin/env python2_' "$file"
  done
  sed -i 's_python$_python2_' desktop/hotrecoll.py
  sed -i 's_python _python2 _' python/recoll/Makefile recollinstall*

  sed -i '1,1i#include <unistd.h>' utils/rclionice.cpp

  QMAKE=qmake-qt4 ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man
  make
}

package() {
  cd "$srcdir/$pkgname-${pkgver/_/}"

  make DESTDIR="$pkgdir" install
}

# vim:set ts=2 sw=2 et:
