# $Id: PKGBUILD 105348 2014-02-05 18:51:35Z angvp $ 
# Maintainer: Angel Velasquez <angvp@archlinux.org> 
# Contributor: Sébastien Luttringer <seblu@aur.archlinux.org>

pkgbase=python-sqlalchemy
pkgname=('python-sqlalchemy' 'python2-sqlalchemy')
pkgver=0.9.2
pkgrel=1
arch=('i686' 'x86_64' 'mips64el') # python2 package contain .so
url="http://www.sqlalchemy.org/"
license=('custom: MIT')
makedepends=('python' 'python2' 'python-setuptools' 'python2-setuptools' 'python-nose' 'python2-nose')
source=("https://pypi.python.org/packages/source/S/SQLAlchemy/SQLAlchemy-$pkgver.tar.gz")
md5sums=('c36a958e46a8514583be82523785269d')

build() {
  cp -a SQLAlchemy-$pkgver SQLAlchemy2-$pkgver
  cd SQLAlchemy-$pkgver
  python setup.py build
  cd ../SQLAlchemy2-$pkgver
  python2 setup.py build
}

check() {
  cd SQLAlchemy-${pkgver}
  python setup.py check
  python2 setup.py check

}

package_python-sqlalchemy() {
  pkgdesc='Python SQL toolkit and Object Relational Mapper'
  depends=('python')
  optdepends=('python-psycopg2: connect to PostgreSQL database')

  cd SQLAlchemy-${pkgver}
  python setup.py install --root="${pkgdir}"
  install -D -m644 LICENSE \
	  "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_python2-sqlalchemy() {
  pkgdesc='Python 2 SQL toolkit and Object Relational Mapper'
  depends=('python2')
  optdepends=('python2-psycopg2: connect to PostgreSQL database')

  cd SQLAlchemy2-$pkgver
  python2 setup.py install --root="$pkgdir"
  install -D -m644 LICENSE \
	  "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

# vim:set ts=2 sw=2 ft=sh et:
