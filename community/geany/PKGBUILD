# $Id: PKGBUILD 99754 2013-10-30 23:30:36Z allan $
# Maintainer: Alexander Rødseth <rodseth@gmail.com>
# Contributor: Angel Velasquez <angvp@archlinux.org> 
# Contributor: Ionut Biru  <ibiru@archlinux.ro>
# Contributor: William Rea <sillywilly@gmail.com>
# Contributor: Allan McRae <mcrae_allan@hotmail.com>

pkgname=geany
pkgver=1.23.1
pkgrel=1
pkgdesc='Fast and lightweight IDE'
arch=('i686' 'x86_64' 'mips64el')
url="http://www.geany.org/"
license=('GPL')
depends=('gtk2' 'hicolor-icon-theme' 'desktop-file-utils')
makedepends=('perlxml' 'setconf' 'intltool')
optdepends=('vte: for terminal support'
            'geany-plugins: various extra features'
            'python2')
install="$pkgname.install"
source=("http://download.geany.org/$pkgname-$pkgver.tar.bz2")
sha256sums=('8815b16e59d8679ec359a1a5754fee05e77f7bca53083c939654bfc77d978fad')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  # Python2 fix
	sed -i '0,/on/s//on2/' data/templates/files/main.py

  # Syntax highlighting for PKGBUILD files
  sed -i 's/Sh=/Sh=PKGBUILD;/' data/filetype_extensions.conf
}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  ./configure --prefix=/usr
  make

  # Fix for FS#10318
  setconf geany.desktop MimeType ''
}

package() {
  cd "$srcdir/$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install
}

# vim:set ts=2 sw=2 et:
