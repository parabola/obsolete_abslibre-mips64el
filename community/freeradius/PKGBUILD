# $Id: PKGBUILD 105909 2014-02-19 08:49:58Z spupykin $
# Maintainer: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Jason R Begley (jayray@digitalgoat.com>

pkgname=freeradius
pkgver=3.0.1
pkgrel=1
pkgdesc="The premier open source RADIUS server"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.freeradius.org/"
license=('GPL')
depends=('krb5' 'pth' 'net-snmp' 'postgresql-libs' 'libmariadbclient' 'talloc')
makedepends=('libpcap' 'unixodbc' 'python2')
optdepends=('libpcap' 'unixodbc' 'python2')
options=('!makeflags')
install=$pkgname.install
source=("ftp://ftp.freeradius.org/pub/radius/freeradius-server-$pkgver.tar.bz2"{,.sig}
	freeradius.tmpfiles
	freeradius.service
	https://github.com/FreeRADIUS/freeradius-server/commit/ff5147c9e5088c7.patch)
md5sums=('40b7533582c3f870af117213e8905958'
         'SKIP'
         'f959e89812bedfc9f8308076f78cd74e'
         'e3f18e3a25df3b692e59f60605354708'
         '9dc9a08bee8b0c3e4822c94f272e1fd6')

prepare() {
  cd $srcdir/freeradius-server-$pkgver
  patch -p1 <$srcdir/ff5147c9e5088c7.patch
}

build() {
  cd $srcdir/freeradius-server-$pkgver
  ./configure --with-system-libtool --with-system-libltdl \
	      --prefix=/usr --enable-heimdal-krb5 \
	      --localstatedir=/var \
	      --sysconfdir=/etc \
	      --sbindir=/usr/bin \
	      --libdir=/usr/lib/freeradius \
	      --with-udpfromto
  make
}

package() {
  cd $srcdir/freeradius-server-$pkgver
  make install R=$pkgdir
  chmod o+r $pkgdir/etc/raddb/*
  mv $pkgdir/etc/raddb $pkgdir/etc/raddb.default
  rm -rf $pkgdir/var/run

  install -Dm0644 $srcdir/$pkgname.service $pkgdir/usr/lib/systemd/system/$pkgname.service
  install -Dm0644 $srcdir/$pkgname.tmpfiles $pkgdir/usr/lib/tmpfiles.d/$pkgname.conf
}
