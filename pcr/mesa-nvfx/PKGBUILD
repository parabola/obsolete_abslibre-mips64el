# $Id: PKGBUILD 198319 2013-10-30 14:08:41Z allan $
# Maintainer: Jan de Groot <jgc@archlinux.org>
# Maintainer: Andreas Radke <andyrtr@archlinux.org>
# Maintainer (Parabola): Márcio Silva <coadde@parabola.nu>

pkgbase=mesa-nvfx
pkgname=('nouveau-nvfx-dri' 'svga-nvfx-dri' 'mesa-nvfx' 'mesa-nvfx-libgl')
pkgver=8.0.5
pkgrel=3
arch=('i686' 'x86_64' 'mips64el')
makedepends=('python2' 'libxml2' 'libx11' 'glproto' 'libdrm-nvfx' 'dri2proto' 'libxxf86vm' 'libxdamage'
             'libvdpau' 'elfutils' 'systemd' 'imake')
url="http://mesa3d.sourceforge.net"
license=('custom')
source=(ftp://ftp.freedesktop.org/pub/mesa/older-versions/${pkgver::1}.x/${pkgver}/MesaLib-${pkgver}.tar.bz2
        LICENSE)
md5sums=('01305591073a76b65267f69f27d635a3'
         '5c65a0fe315dd347e09b1f2826a1df5a')

build() {
  cd ${srcdir}/?esa-*

  autoreconf -vfi # our automake is far too new for their build system :)

  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --with-dri-driverdir=/usr/lib/xorg/modules/dri \
    --with-gallium-drivers=nouveau,svga,swrast \
    --with-dri-drivers=swrast \
    --with-egl-platforms=x11,drm \
    --with-llvm-shared-libs \
    --disable-gallium-llvm \
    --enable-egl \
    --enable-gallium-egl \
    --enable-shared-glapi \
    --enable-gbm \
    --enable-glx-tls \
    --enable-dri \
    --enable-glx \
    --enable-osmesa \
    --enable-gles1 \
    --enable-gles2 \
    --enable-texture-float \
    --enable-xa \
    --enable-vdpau \
    --disable-glu
    # --help

  make

  # fake installation
  mkdir $srcdir/fakeinstall
  make DESTDIR=${srcdir}/fakeinstall install
}

package_nouveau-nvfx-dri() {
  pkgdesc="Mesa drivers for Nouveau NVFX [NV30 and NV40] (GeForce FX - 7000 series card) video card family (without wayland support)"
  optdepends=('libtxc_dxtn: S3 Texture Compressed support')
  depends=("mesa-nvfx-libgl=${pkgver}")
  provides=('nouveau-dri')
  conflicts=('nouveau-dri')

  install -m755 -d ${pkgdir}/usr/lib/vdpau/
  mv -v ${srcdir}/fakeinstall/usr/lib/vdpau/libvdpau_nouveau.* ${pkgdir}/usr/lib/vdpau/

  install -m755 -d ${pkgdir}/usr/lib/xorg/modules/dri
  mv -v ${srcdir}/fakeinstall/usr/lib/xorg/modules/dri/nouveau_dri.so ${pkgdir}/usr/lib/xorg/modules/dri/
  
  install -m755 -d ${pkgdir}/usr/lib/gallium-pipe
  mv -v ${srcdir}/fakeinstall/usr/lib/gbm/pipe_nouveau* ${pkgdir}/usr/lib/gallium-pipe/
  
  # vdpau drivers are buggy with nouveau NVFX, so remove them (FS#36754)
  rm -rf ${pkgdir}/usr/lib/vdpau/

  install -m755 -d "${pkgdir}/usr/share/licenses/nouveau-nvfx-dri"
  install -m644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/nouveau-nvfx-dri/"
}

package_svga-nvfx-dri() {
  pkgdesc="Gallium3D VMware guest GL driver for Nouveau NVFX [NV30 and NV40] (GeForce FX - 7000 series card family (without wayland support)"
  depends=('libdrm-nvfx' 'expat')
  provides=('svga-dri')
  conflicts=('svga-dri')

  install -m755 -d ${pkgdir}/usr/lib/xorg/modules/dri
  mv -v ${srcdir}/fakeinstall/usr/lib/xorg/modules/dri/vmwgfx_dri.so ${pkgdir}/usr/lib/xorg/modules/dri/

  install -m755 -d ${pkgdir}/usr/lib/gallium-pipe
  mv -v ${srcdir}/fakeinstall/usr/lib/gbm/pipe_vmwgfx* ${pkgdir}/usr/lib/gallium-pipe/
  
  install -m755 -d "${pkgdir}/usr/share/licenses/svga-nvfx-dri"
  install -m644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/svga-nvfx-dri/"
}

package_mesa-nvfx() {
  pkgdesc="an free implementation of the OpenGL specification for Nouveau NVFX [NV30 and NV40] (GeForce FX - 7000 series card) video card family (without wayland and support)"
  depends=('libdrm-nvfx' 'libvdpau' 'libxxf86vm' 'libxdamage' 'systemd' 'elfutils')
  optdepends=('opengl-man-pages: for the OpenGL API man pages')
  provides=('mesa' 'libegl' 'libgbm' 'libgles')
  conflicts=('mesa' 'libegl' 'libgbm' 'libgles')

  mv -v ${srcdir}/fakeinstall/* ${pkgdir}
  # rename libgl.so
  mv ${pkgdir}/usr/lib/libGL.so.1.2 	${pkgdir}/usr/lib/mesa-libGL.so.1.2.0
  rm ${pkgdir}/usr/lib/libGL.so{,.1}

  # removing glu headers files
  rm -v "${pkgdir}/usr/include/GL/glu"{,_mangle}".h"

  install -m755 -d "${pkgdir}/usr/share/licenses/mesa-nvfx"
  install -m644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/mesa-nvfx/"
}

package_mesa-nvfx-libgl() {
  pkgdesc="Mesa 3-D graphics library for Nouveau NVFX [NV30 and NV40] (GeForce FX - 7000 series card) video card family (without wayland support)"
  depends=("mesa-nvfx=${pkgver}")
  provides=('mesa-libgl' 'libgl')
  conflicts=('mesa-libgl' 'libgl')

  install -m755 -d "${pkgdir}/usr/lib"

  ln -s mesa-libGL.so.1.2.0      ${pkgdir}/usr/lib/libGL.so
  ln -s mesa-libGL.so.1.2.0      ${pkgdir}/usr/lib/libGL.so.1
  ln -s mesa-libGL.so.1.2.0      ${pkgdir}/usr/lib/libGL.so.1.2
  ln -s mesa-libGL.so.1.2.0      ${pkgdir}/usr/lib/libGL.so.1.2.0

  install -m755 -d "${pkgdir}/usr/share/licenses/mesa-nvfx-libgl"
  install -m644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/mesa-nvfx-libgl/"
}
