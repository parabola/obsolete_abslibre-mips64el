# Contributor: Christian Hesse <mail@eworm.de>
# Contributor: Thomas Dziedzic < gostrc at gmail >
# Contributor: mickele
# Contributor: marcus fritzsch <fritschy@googlemail.com>
# Maintainer : Parabola GNU / Linux-libre Aurelien Desbrières <aurelien@cwb.IO>

pkgname=coin
pkgver=3.1.3
pkgrel=9
pkgdesc='A high-level 3D graphics toolkit on top of OpenGL'
url='http://www.coin3d.org/'
license=('GPL')
arch=('i686' 'x86_64')
depends=('mesa' 'expat')
makedepends=('doxygen')
optdepends=('openal: sound/dynamic linking support'
            'fontconfig: dynamic linking support'
            'zlib: dynamic linking support'
            'freetype2: dynamic linking support'
            'js: dynamic linking support'
	    'simage: image format support')
options=('!libtool')
source=("https://bitbucket.org/Coin3D/coin/downloads/Coin-${pkgver}.tar.gz")

build() {
  cd Coin-${pkgver}

  # fix http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=667139
  sed -i "/#include <Inventor\/C\/basic.h>/i #include <Inventor/C/errors/debugerror.h>" include/Inventor/SbBasic.h

  ./configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --enable-optimization \
    --enable-3ds-import \
    --enable-javascript-api \
    --enable-threadsafe \
    --enable-exceptions \
    --enable-man \
    --with-mesa \
    --disable-debug \
    --enable-shared \
    --disable-maintainer-mode \
    --disable-dependency-tracking \
    --enable-system-expat

  make
}

package() {
  cd Coin-${pkgver}

  make DESTDIR=${pkgdir} install

  # final adjustments
  for _FILE in threads deprecated errors events ; do
    mv  "${pkgdir}/usr/share/man/man3/${_FILE}.3" "${pkgdir}/usr/share/man/man3/coin-${_FILE}.3"
  done
}
