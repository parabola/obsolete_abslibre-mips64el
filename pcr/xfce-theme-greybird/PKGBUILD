# Maintainer: Limao Luo <luolimao+AUR@gmail.com>
# Contributor: flan_suse

# This suite contains the following:
#        * xfwm4 theme
#        * xfwm4 compact theme
#        * metacity theme
#        * emerald theme
#        * gtk2 theme
#        * gtk2 theme (classic)
#        * gtk3 theme
#        * gtk3 theme (classic)
#        * unity theme
#        * xfce4-notify theme
#        * lightdm theme
#        * wallpaper (found in the shimmer-wallpapers package; optdepends)

# There have been some drastic changes as of July 22nd, 2012!
# Please read through the AUR page comments if you wish to learn more.
# I have modified this PKGBUILD to compensate for the drastic changes.

pkgname=xfce-theme-greybird
_pkgname=Greybird
pkgver=1.1.1
_pkgverclassic=0.8.2
pkgrel=1
pkgdesc="A grey and blue Xfce theme, used by default in Xubuntu 12.04; includes the classic low saturation theme"
arch=(any)
url=http://shimmerproject.org/projects/greybird/
license=(CCPL:by-sa-3.0 GPL)
groups=(xfce-themes-shimmer-collection)
depends=(gtk-engine-murrine)
optdepends=('elementary-xfce-icons: matching icon set; use the dark icon theme'
    'gtk-engine-unico: required for gtk3 support for the classic theme; not required for the current theme'
    'gtk3: required for CSS/GTK3 theme'
    'lightdm-gtk-greeter: required for the LightDM GTK theme'
    'lightdm-unity-greeter: required for the LightDM Unity theme'
    'shimmer-wallpapers: contains the Greybird wallpaper, among others')
conflicts=($pkgname-git)
source=($pkgname-$pkgver.tar.gz::https://github.com/shimmerproject/$_pkgname/archive/v$pkgver.tar.gz
    $pkgname-$_pkgverclassic.tar.gz::https://github.com/shimmerproject/$_pkgname/archive/v$_pkgverclassic.tar.gz)
sha256sums=('374bc55d6af98ca6052cc5dd6c55db3b1ce9002fe81d3fde3e9b72febe697878'
    'b63da3527760c194bb7299c1cb45297268439740e52e2c5e1f39fc31dce6293a')
sha512sums=('1e4f7dea5e0206972d7d4a5bf42b353abf21e40d215fc1b0e7b546a4c95c7ce0604a37e9f2ad6c272fedf4720d0a5eee732a6b89d83c635c24082c24711c9e5e'
    '09f19bbfe02a477de6635198736897d143a5eec8ed13e6d1863c80f5d78b341ecdf2b2cf48bffddead25082eeadde475d360fc7bee773307c5cc10fb3ff90034')

package() {
    local _themedir="$pkgdir/usr/share/themes"
    install -d "$_themedir/$_pkgname "{Classic,Compact}

    cp -rf $_pkgname-$pkgver/ "$_themedir"/$_pkgname/
    rm "$_themedir"/$_pkgname/.gitignore
    cp -rf $_pkgname-$_pkgverclassic/gtk-{2,3}.0 "$_themedir/$_pkgname Classic"
    ln -s /usr/share/themes/$_pkgname/xfwm4_compact "$_themedir/$_pkgname Compact/xfwm4"
}
