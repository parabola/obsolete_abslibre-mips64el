# Puppet: Installer: Arch
# Contributor: Greg Sutcliffe <puppet-aur (at) emeraldreverie (dot) org>
# Contributor: Hyacinthe Cartiaux <hyacinthe (dot) cartiaux (at) free (dot) fr>
# Contributor: Thomas S Hatch <thatch45 (at) Gmail (dot) com>
# Contributor: Xavion <Xavion (dot) 0 (at) Gmail (dot) com>
# Contributor: Miah Johnson <miah (at) chia-pet dot org>
# Contributor: Dave Simons <miouhpi (at) Gmail (dot) com>
# Contributor: Niels Abspoel <aboe76 (at) Gmail (dot) com>
# Maintainer : Parabola GNU / Linux-libre Aurelien Desbrieres <aurelien@cwb.io>

# RC style, reserved for later use
#pkgname=puppet
#_rc=8
#_pkgver=3.0.0
#pkgver=${_pkgver}_rc${_rc}

pkgname=puppet
pkgver=3.0.1
pkgrel=5
pkgdesc="A system for automating system administration tasks."
arch=("any")
url="http://puppetlabs.com/projects/puppet/"
license=("APACHE")
depends=("ruby" "ruby-shadow" "facter" "ruby-hiera" "ruby-hiera-json")
makedepends=('which')
backup=("etc/puppet/puppet.conf")
options=(emptydirs)
provides=("puppet" "vim-puppet" "ruby-hiera-puppet")
conflicts=("puppet3" "ruby-puppet" "vim-puppet" "ruby-hiera-puppet")
install="puppet.install"
source=(http://puppetlabs.com/downloads/puppet/puppet-${pkgver//_/-}.tar.gz
        fileserver.conf
        puppet
        puppet.conf
        puppetmaster)
md5sums=('2117e3a1964ac54fcd36d926eed3db94'
         'ffacd9bfb9c7656a3a345dbc5eb0e67a'
         '59dbf39e251bc4877e7604a5876c642d'
         'a7d61d20e46b411eb50ad5279987584a'
         '4c507c580cb28d7a5adc9ea6b3626657')

package() {
  cd $srcdir/puppet-${pkgver//_/-}

  # Install
  ruby ./install.rb \
    --destdir=$pkgdir \
    --bindir=/usr/bin \
    --sbindir=/usr/sbin \
    --mandir=/usr/share/man


  # Set up vim and emacs
  install -Dp -m0644 ext/vim/ftdetect/puppet.vim $pkgdir/usr/share/vim/vimfiles/ftdetect/puppet.vim
  install -Dp -m0644 ext/vim/syntax/puppet.vim $pkgdir/usr/share/vim/vimfiles/syntax/puppet.vim
  install -Dp -m0644 ext/emacs/puppet-mode.el $pkgdir/usr/share/emacs/site-lisp/puppet-mode.el
  install -Dp -m0644 ext/emacs/puppet-mode-init.el $pkgdir/usr/share/emacs/site-lisp/site-start.d/puppet-mode-init.el

  # Configuration and rc scripts
  install -d $pkgdir/etc/{puppet,rc.d}
  install -m 644 $srcdir/puppet.conf $pkgdir/etc/puppet/puppet.conf
  install -m 644 $srcdir/fileserver.conf $pkgdir/etc/puppet/fileserver.conf
  
  # Setup tmpfiles.d config
  install -d $pkgdir/etc/tmpfiles.d
  echo "D /var/run/puppet 0755 puppet puppet -" > $pkgdir/etc/tmpfiles.d/puppet.conf

  # Configuration for hiera / symlink is there to use hiera within puppet.
  ln -snf /etc/hiera.yaml ${pkgdir}/etc/puppet/hiera.yaml

  # Systemd files
  install -Dm 644 $srcdir/puppet-${pkgver//_/-}/ext/systemd/puppetagent.service $pkgdir/usr/lib/systemd/system/puppetagent.service
  install -Dm 644 $srcdir/puppet-${pkgver//_/-}/ext/systemd/puppetmaster.service $pkgdir/usr/lib/systemd/system/puppetmaster.service

  # Create puppet homedir and hieradir
  install -d $pkgdir/var/lib/puppet
  install -d $pkgdir/var/lib/hiera

  install -D $srcdir/puppet $pkgdir/etc/rc.d/puppet
  install -D $srcdir/puppetmaster $pkgdir/etc/rc.d/puppetmaster

  # Copy the LICENSE file in the usual directory
  install -d -m755 "${pkgdir}/usr/share/licenses/${pkgname}"
  install -m 644 "LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

}
