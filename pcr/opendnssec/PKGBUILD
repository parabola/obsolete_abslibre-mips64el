# Maintainer: Javier Torres <javitonino [at] gmail [dot] com>

pkgname=opendnssec
pkgver=1.4.0
pkgrel=3
pkgdesc="Turn-key solution for DNSSEC (sqlite3)"
arch=('i686' 'x86_64' 'mips64el')
url="http://www.opendnssec.org/"
license=('BSD')
depends=('libxml2' 'ldns' 'sqlite3' 'softhsm')
backup=("etc/opendnssec/conf.xml"
        "etc/opendnssec/kasp.xml"
        "etc/opendnssec/zonelist.xml")
install="opendnssec.install"
source=("http://www.opendnssec.org/files/source/${pkgname}-${pkgver}.tar.gz"{,.sig}
        "pid-path.patch"
        "ods-signer.service"
        "ods-enforcer.service")
sha256sums=('36d4926dcdf351a527ad7600b151ab6cc56d0a472a7eb8871eecd70afef9e101'
            'SKIP'
            '487a4c05a07feb97c865ddc4c13d00eea6ce8b2b1e5031983c15484f4991ebed'
            '1a8cdec0e97a33048956268b766da570f8f7a90d05af59e547219f1381832071'
            '75cecbfb0ece13957a68a5bc39c20a1d69b95373e7473545d70621e1732733d8')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  # /var/lib/run -> /var/run
  patch -p0 -i "${srcdir}/pid-path.patch"

  aclocal
  autoconf
  automake --add-missing
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  ./configure --prefix=/usr \
              --datarootdir=/usr/share \
              --localstatedir=/var/lib \
              --sysconfdir=/etc \
              --with-pkcs11-softhsm=/usr/lib/libsofthsm.so \
              --sbindir=/usr/bin

  # Create the correct (/var/run/opendnssec) directory
  sed -i \
  -e "s/\$(INSTALL) -d \$(DESTDIR)\$(localstatedir)\/run\/opendnssec/#Removed/" \
  -e "s/\$(INSTALL) -d \$(DESTDIR)\$(localstatedir)\/run/\$(INSTALL) -d \$(DESTDIR)\$(OPENDNSSEC_PID_DIR)/" \
  Makefile

  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  install -d "${pkgdir}/var/run/opendnssec"

  make DESTDIR="${pkgdir}" install

  install -d "${pkgdir}/etc/rc.d"
  install -Dm0644 "${srcdir}/ods-signer.service" \
        "${pkgdir}/usr/lib/systemd/system/ods-signer.service"
  install -Dm0644 "${srcdir}/ods-enforcer.service" \
        "${pkgdir}/usr/lib/systemd/system/ods-enforcer.service"
  install -Dm0644 "LICENSE" \
        "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  install -Dm0644 "enforcer/utils/migrate_adapters_1.sqlite3" \
        "${pkgdir}/usr/share/opendnssec"

  chown -R 227:227 "${pkgdir}/etc/opendnssec" \
                   "${pkgdir}/var/lib/opendnssec" \
                   "${pkgdir}/var/run/opendnssec" 
  chmod 750 "${pkgdir}/etc/opendnssec" \
            "${pkgdir}/var/lib/opendnssec" \
            "${pkgdir}/var/run/opendnssec" 
}
