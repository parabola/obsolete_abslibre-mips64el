# Maintainer: Thomas Dziedzic <gostrc@gmail.com>
# Contributor: Allan McRae <allan@archlinux.org>
# Contributor: John Proctor <jproctor@prium.net>
# Contributor: Jeramy Rutley <jrutley@gmail.com>

_pkgname=ruby
pkgname=("${_pkgname}1" "${_pkgname}1-docs")
pkgver=1.9.3_p448
pkgrel=2
arch=('i686' 'x86_64' 'mips64el')
url='http://www.ruby-lang.org/en/'
license=('BSD' 'custom')
makedepends=('gdbm' 'openssl' 'tk' 'libffi' 'doxygen' 'graphviz' 'libyaml')
options=('!emptydirs' '!makeflags' '!staticlibs')
source=("ftp://ftp.ruby-lang.org/pub/ruby/${pkgver%.*}/ruby-${pkgver//_/-}.tar.bz2"
        'gemrc')
md5sums=('aa710d386e5903f78f0231868255e6af'
         '6fb8e7a09955e0f64be3158fb4a27e7a')

build() {
  cd ruby-${pkgver//_/-}

  msg 'use gdbm because db v6 changed the license to AGPL'
  sed -i 's|db db2 db1 db5 db4 db3||
          \|db[1-5]/n\{0,1\}dbm\{0,1\}[.]h|d
          \|traditional ndbm [(]4[.]3BSD[)]|d
         ' ext/dbm/extconf.rb

  msg 'fixing gemrc to gem1rc'
  sed -i 's|gemrc|gem1rc|g
         ' doc/rubygems/{ChangeLog,History.txt} \
           lib/rubygems{,/command,/commands/environment_command,/config_file}.rb \
           test/rubygems/test_gem_{config_file,gem_runner,remote_fetcher}.rb

  PKG_CONFIG=/usr/bin/pkg-config ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --enable-shared \
    --disable-rpath \
    --program-suffix=1

  make
}

check() {
  cd ruby-${pkgver//_/-}

  make test
}

package_ruby1() {
  pkgdesc='An object-oriented language for quick and easy programming (1.9 ver.)'
  depends=('gdbm' 'openssl' 'libffi' 'libyaml')
  optdepends=('tk: for Ruby/TK'
              'ruby1-docs: Ruby documentation')
  backup=('etc/gem1rc')
  install='ruby.install'

  cd ruby-${pkgver//_/-}

  make DESTDIR="${pkgdir}" install-nodoc

  install -D -m644 ${srcdir}/gemrc "${pkgdir}/etc/gem1rc"

  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/ruby1/LICENSE"
  install -D -m644 BSDL "${pkgdir}/usr/share/licenses/ruby1/BSDL"

  msg 'remove useless files'
  rm -v "${pkgdir}/usr/lib/libruby.so"
}

package_ruby1-docs() {
  pkgdesc='Documentation files for ruby (1.9 ver.)'

  cd ruby-${pkgver//_/-}

  make DESTDIR="${pkgdir}" install-doc install-capi

  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/ruby1-docs/LICENSE"
  install -D -m644 BSDL "${pkgdir}/usr/share/licenses/ruby1-docs/BSDL"

  msg 'fixing doc path'
  mv -v ${pkgdir}/usr/share/doc/ruby{,1}
}
