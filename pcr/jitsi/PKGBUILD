# Maintainer: Xavier Devlamynck <magicrhesus@ouranos.be>
# Contributors: Keshav P R, atommix aka Aleks Lifey, Xavion, Ananda Samaddar, Dan Serban, Xyne
# Maintainer (Parabola): André Silva <emulatorman@parabola.nu>
# Contributor (Parabola): Márcio Silva <coadde@lavabit.com>

pkgname=jitsi
pkgver=2.4.4997
pkgrel=1
pkgdesc="An audio/video SIP VoIP phone and instant messenger written in Java (formerly SIP-Communicator)"
arch=('i686' 'x86_64' 'mips64el')
url="http://jitsi.org"
license=('LGPL')
depends=('jre7-openjdk')
makedepends=('apache-ant' 'jdk7-openjdk')
options=(!strip !emptydirs zipman !libtool docs)
source=("http://download.jitsi.org/jitsi/src/jitsi-src-${pkgver}.zip"
		jitsi.desktop
		jitsi.sh
		drop-open-term-on-index-jn-html.patch)
sha256sums=('f1c2688d7d6bf1916fed3b8b105a785662980c5b297dcab3c9e7d272647ef825'
            '61e3bec3470790fa067f87d978016ec4452a6fd3dfba2c9afa5245b58d3cb19d'
            '529722efc3bae6cb8923680363a4d778ccf78db9caf20bee90a110d34181d9f5'
            'a862465431e9d272e00bb9aafe56edec0d5c4f3718062add53972685adb90631')

prepare() {
  cd $srcdir/$pkgname

  # append the build revision to the jitsi version
  sed -i "s|0.build.by.SVN|build.$pkgver|" src/net/java/sip/communicator/impl/version/NightlyBuildID.java

  # remove Open Source term
  patch -Np0 -i ../drop-open-term-on-index-jn-html.patch
  sed -i 's|Open Source / ||;
          s|open source / ||;
          s|Open Source|Free Software|;
          s|open source|free software|;
         ' $(grep -ril 'open source')
  sed -i 's|OpenSource / ||;
          s|opensource / ||;
          s|OpenSource|FreeSoftware|;
          s|opensource|freesoftware|;
         ' $(grep -ril opensource)

  # remove Linux term
  sed -i '/, Linux/ s|Linux|GNU/Linux|;
          /And Linux/ s|Linux|GNU/Linux|;
          /and Linux/ s|Linux|GNU/Linux|;
          /For Linux/ s|Linux|GNU/Linux|;
          /for Linux/ s|Linux|GNU/Linux|;
          /Linux,/ s|Linux|GNU/Linux|;
          /Linux and/ s|Linux|GNU/Linux|;
          /Linux distribution/ s|Linux|GNU/Linux|;
          /Linux machine/ s|Linux|GNU/Linux|;
          /Linux operating/ s|Linux|GNU/Linux|;
          /Linux OS/ s|Linux|GNU/Linux|;
          /Linux specific/ s|Linux|GNU/Linux|;
          /Linux system/ s|Linux|GNU/Linux|;
          /Under Linux/ s|Linux|GNU/Linux|;
          /under Linux/ s|Linux|GNU/Linux|;
          /On Linux/ s|Linux|GNU/Linux|;
          /on Linux/ s|Linux|GNU/Linux|;
          s|GNU/GNU|GNU|;
          s|GNU/Linux kernel|Linux kernel|;
          s|GNU/Linux specific kernel|Linux specific kernel|;
         ' $(grep -rl Linux)
}

build() {
  cd $srcdir/$pkgname
  . /etc/profile.d/apache-ant.sh
  ant rebuild
}

package() {
  cd $srcdir/$pkgname
  find lib/ lib/bundle/ -maxdepth 1 -type f -exec install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/"{} \;
#  find lib/os-specific/linux/ -maxdepth 1 -type f -execdir install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/lib/"{} \;
  shopt -sq extglob
  find lib/native/linux$(sed 's/_/-/g' <<<${CARCH/#*(i?86|x86)/})/ -maxdepth 1 -type f -execdir install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/lib/native/"{} \;
  find sc-bundles/{,os-specific/linux/} -maxdepth 1 -type f -execdir install -Dm644 {} "${pkgdir}/usr/lib/${pkgname}/sc-bundles/"{} \;
  install -Dm755 "${srcdir}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
  install -Dm644 "${srcdir}/${pkgname}.desktop" "${pkgdir}/usr/share/applications/${pkgname}.desktop"

  cd "resources/install/debian/"
  for _file in *.{svg,xpm}; do
    install -Dm644 "$_file" "${pkgdir}/usr/share/pixmaps/${_file}"
  done
}
