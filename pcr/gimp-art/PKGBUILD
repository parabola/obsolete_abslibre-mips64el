# Maintainer: Márcio Silva <coadde@lavabit.com>

pkgname=gimp-art
pkgver=2.8.2
pkgrel=2
pkgdesc='GNU Image Manipulation Program (changed splash artwork)'
arch=(
  i686
  x86_64
  mips64el
)
url=http://www.${pkgname%-art}.org/
license=(
  GPL
  LGPL
)
depends=(
  babl
  dbus-glib
  desktop-file-utils
  gegl
  hicolor-icon-theme
  jasper
  lcms
  libexif
  libmng
  librsvg
  libxmu
  libxpm
  libwmf
  pygtk
)
makedepends=(
  alsa-lib
  curl
  ghostscript
  intltool
  iso-codes
  libwebkit
  poppler-glib
)
optdepends=(
  'alsa-lib: for MIDI event controller module'
  'curl: for URI support'
  'ghostscript: for postscript support'
  'gutenprint: for sophisticated printing only as gimp has built-in cups print support'
  'libwebkit: for the help browser'
  'poppler-glib: for pdf support'
)
options=(
  !libtool
  !makeflags
)
conflicts=(
  ${pkgname%-art}
  ${pkgname%-art}-devel
)
provides=(
  ${pkgname%-art}=$pkgver
)
install=${pkgname%-art}.install
source=(
  ftp://ftp.${pkgname%-art}.org/pub/${pkgname%-art}/v${pkgver%.*}/${pkgname%-art}-$pkgver.tar.bz2
  ${pkgname%-art}-splash-artwork.png
  ${pkgname%-art}-splash-artwork.license
)
sha512sums=(
  82a15072aba97d41a7dd74920f5db4e89442ff83841ac614f6fadfd964bdb8d3ae478ac0ba2e906bcf4569c8de00a1616fd6d626032f093060f28df3655d9111
  1a2930e612b339f49aa85f98275c24202e056f2fff5c1fda94e662bfd45ed5b6226e6ab1947f3daf758f4e5106fd3f7e4f286d63b258f008b6a5d9e5cd6f0611
  d3f055ce278f25b33a6b11f3b031ae801206390fa1f62e3383daf35570e5f68b98ddfac30dea71d89d4916eaf3263e642ad06805c3175f620412e546784335ac
)

build() {
  cd $srcdir/${pkgname%-art}-$pkgver

  ## Add artwork splash image ##
  install -Dm644 $srcdir/${pkgname%-art}-splash-artwork.png \
    $srcdir/${pkgname%-art}-$pkgver/data/images/${pkgname%-art}-splash.png

  PYTHON=/usr/bin/python2 ./configure --prefix=/usr \
    --enable-${pkgname%-art}-console \
    --enable-mp \
    --enable-python \
    --sysconfdir=/etc \
    --with-gif-compression=lzw \
    --with-libcurl \
    --without-aa \
    --without-gvfs
  make
}

package() {
  cd $srcdir/${pkgname%-art}-$pkgver
  make DESTDIR=$pkgdir install
  sed -i 's|#!/usr/bin/env python|#!/usr/bin/env python2|' $pkgdir/usr/lib/${pkgname%-art}/${pkgver::2}0/plug-ins/*.py

  ## Add artwork splash license ##
  install -Dm644 $srcdir/${pkgname%-art}-splash-artwork.license \
    $pkgdir/usr/share/licenses/${pkgname%-art}/${pkgname%-art}-splash-artwork.license

  rm $pkgdir/usr/share/man/man1/$_pkgname-console.1
  ln -s  ${pkgname%-art}-console-${pkgver%.*}.1.gz $pkgdir/usr/share/man/man1/${pkgname%-art}-console.1.gz
  ln -s  ${pkgname%-art}tool-${pkgver::2}0         $pkgdir/usr/bin/${pkgname%-art}tool
  ln -sf ${pkgname%-art}tool-${pkgver::2}0.1.gz    $pkgdir/usr/share/man/man1/${pkgname%-art}tool.1.gz
}
