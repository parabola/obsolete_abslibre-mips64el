# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>

pkgname=java-json-android
_pkgver=4.2.2_r1
pkgdesc="Java org.json implementation developed as part of Android's libcore"
url="https://android.googlesource.com/platform/libcore/"
license=(Apache2)

pkgver=${_pkgver//_/.}
provides=(java-json)
conflicts=(java-json)

pkgrel=3
arch=(any)

depends=(java-runtime)
makedepends=(java-environment junit)

source=("libre://android-libcore-$_pkgver.tar.gz")
md5sums=('546a4614552615a12f45b62db27acddf')

mkdepends=(git)
mksource=("android-libcore-$_pkgver::git+https://android.googlesource.com/platform/libcore/#tag=android-${_pkgver}")
mkmd5sums=('SKIP')

build() {
  cd "$srcdir"/android-libcore-$_pkgver
  sed 11q < NOTICE > json/NOTICE
  cd json/src/main/java
  javac org/json/*.java
  jar cf json.jar org/json/*.class
}

check() {
  cd "$srcdir"/android-libcore-$_pkgver/json/src/test/java

  # disable test for known bug:
  sed -i 's/test64BitHexValues()/x&/' org/json/ParsingTest.java

  local cp="/usr/share/java/junit.jar:../../main/java/json.jar"
  javac -cp "$cp" org/json/*.java
  printf '%s\n' org/json/*.java | sed 's|/|.|g;s|\.java$||' > tests.txt
  java -cp "$cp:." org.junit.runner.JUnitCore $(cat tests.txt)
}

package() {
  cd "$srcdir"/android-libcore-$_pkgver/json
  install -Dm644 NOTICE "$pkgdir"/usr/share/licenses/$pkgname/NOTICE
  install -Dm644 src/main/java/json.jar "$pkgdir"/usr/share/java/json.jar
}
