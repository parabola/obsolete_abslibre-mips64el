# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>
# Maintainer (AUR): PyroPeter <googlemail.com@abi1789>

pkgname=arduino
pkgver=1.0.2
pkgrel=1
pkgdesc="Arduino SDK and IDE"
arch=('i686' 'x86_64')
url="http://arduino.cc/en/Main/Software"
options=(!strip)
license=('GPL')

depends=('avr-gcc' 'avr-libc' 'libusb-compat' 'java-runtime' 'avrdude'
    'java-rxtx' 'antlr2' 'eclipse-ecj' 'jna')
makedepends=(java-environment apache-ant makepkg-git)
install="arduino.install"
source=("http://arduino.googlecode.com/files/arduino-$pkgver-src.tar.gz"
	'avrlibc-signals.patch'
	'external-avrtools.patch'
	'arduino.png'
	'arduino.desktop')

unset _JAVA_OPTIONS
. /etc/profile.d/jdk.sh

prepare() {
  cd "$srcdir/arduino-$pkgver"
  find . -name '*.hex' -delete
  find . -name '*.jar' -delete
  find . -name '*.so'  -delete
  find . -name '*.swp' -delete
  rm -f build/linux/dist/tools/avrdude*
  rm -rf hardware/arduino/firmwares/wifishield
}

_link_jars() {
  for jar in RXTXcomm.jar antlr2.jar ecj.jar jna.jar; do
    ln -sf /usr/share/java/$jar .
  done
}

_patch() {
  cd "$srcdir/arduino-$pkgver"

  # Fix issue 955 to be compatible with newer versions of avr-libc
  # https://code.google.com/p/arduino/issues/detail?id=955
  # Note: this patch is not the patch given in the comments
  patch -Np1 -i "$srcdir/avrlibc-signals.patch"
  grep -rl 'SIGNAL\s*(' . | xargs sed -ri 's/SIGNAL\s*\(([^)]*_vect)\)/ISR(\1)/g'

  # Don't look for pre-compiled avr-tools
  patch -Np1 -i "$srcdir/external-avrtools.patch"

  rm -rf app/src/processing/app/macosx

  cd app/lib
  _link_jars
}

build() {
  _patch

  # build submodules
  for submodule in core/methods core/preproc; do
    cd "$srcdir/arduino-$pkgver/$submodule"
    ant
  done

  # build the main package
  cd "$srcdir/arduino-$pkgver/build"
  log=`mktemp`
  ant 2>&1|tee "$log"
  if egrep -q 'Error|FAILED' "$log"; then
    rm "$log"
    false
  else
    rm "$log"
  fi

  # symlink jar files to the system locations
  cd "linux/work/lib"
  _link_jars
}

package() {
  cd "$srcdir/arduino-$pkgver/build/linux/work"

  install -d "$pkgdir"/usr/{bin,share/{doc,applications,pixmaps}}

  # copy the whole SDK to /usr/share/arduino/
  cp -r . "$pkgdir/usr/share/arduino"

  # at least support the FHS a little bit:
  ln -s ../share/arduino/arduino "$pkgdir/usr/bin/arduino"
  ln -s ../arduino/reference     "$pkgdir/usr/share/doc/arduino"

  # desktop icon
  install -m644 "$srcdir/arduino.desktop" "$pkgdir/usr/share/applications/"
  install -m644 "$srcdir/arduino.png" "$pkgdir/usr/share/pixmaps/"
}

md5sums=('dfb64e2a7de4a4a8b872732fe90e3249'
         'e44975d31aa1770e4f5ac6e6867b0864'
         '9c696c2361d57027be41ae64436182aa'
         '9e36d33891d5e68d38ec55d1494499a5'
         'eebc4d6495864bea99ad057af801afb9')
