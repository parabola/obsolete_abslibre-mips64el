# Maintainer: Luke Shumaker <lukeshu@sbcglobal.net>

_pkgname=asm
pkgname=java-asm${pkgver%%.*}
pkgdesc="An all purpose Java bytecode manipulation and analysis framework."
url="http://asm.ow2.org/"
license=('custom:BSD3')

arch=('any')
depends=('java-runtime')
makedepends=(
  apache-ant
  java-ow-util-ant-tasks
  jh
)
source=("libre://$pkgname-$pkgver.tar.gz")
mksource=("http://download.forge.objectweb.org/$_pkgname/$_pkgname-$pkgver.tar.gz")

_distdir="$_pkgname-$pkgver/output/dist"
_ow_util_ant_tasks='/usr/share/java/ow_util_ant_tasks.jar'

build() {
  cd "$srcdir/$_pkgname-$pkgver"

  echo "objectweb.ant.tasks.path $_ow_util_ant_tasks" >> build.properties
  ant dist

  # This will make package() easier:
  cd "$srcdir/$_distdir/lib"
  ln -sf all/* .
}

package() {
  # Install license file
  cd "$srcdir/$_pkgname-$pkgver"
  install -Dm644 LICENSE.txt "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  # Install documentation
  cd "$srcdir/$_distdir"
  install -d "$pkgdir/usr/share/doc/$pkgname"
  cp -r doc/javadoc/user "$pkgdir/usr/share/doc/$pkgname/javadoc"
  cp -r examples         "$pkgdir/usr/share/doc/$pkgname/examples"

  # Install the README about the -all files
  cd "$srcdir/$_distdir/lib"
  install -Dm644 README.txt \
      "$pkgdir"/usr/share/java/$_pkgname/README-$pkgver.txt

  # Install Maven artifacts
  export DESTDIR=$pkgdir
  for artifact in $(printf '%s\n' *.pom|sed 's/-[0-9.]*\.pom$//'); do
    if [[ -f $artifact-$pkgver.jar ]]; then
      # This artifact has a jar file
      jh mvn-install $_pkgname $artifact $pkgver $artifact-$pkgver.{pom,jar}
      # Symlink them to /usr/share/java
      ln -s "$(jh mvn-basename asm $artifact $pkgver).jar" \
	"$pkgdir"/usr/share/java/$_pkgname/
      ln -s $_pkgname/$artifact-$pkgver.jar \
	"$pkgdir"/usr/share/java/$artifact-${pkgver%%.*}.jar
    else
      # This artifact is just a pom
      jh mvn-install $_pkgname $artifact $pkgver $artifact-$pkgver.pom
    fi
  done
}
